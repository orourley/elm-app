export type FormValue = string | boolean | FileList | null;
export type FormValues = { [key: string]: (FormValue | FormValues | (FormValue | FormValues)[]); };

export class FormHelper {

	/** Gets the current input values as an object. Handles nested objects (tokens) separated by a period, and arrays designated by
	 *  a [] or [+] at the end of the token. The suffix [+] means that it is the start of a new item in the array. */
	static getValues(elem: Element): FormValues {

		// Gather all input values in a big array.
		const formElems: NodeListOf<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> = elem.querySelectorAll('input, textarea, select');

		const values: FormValues = {};
		for (const formElem of formElems) {

			// Get the name.
			const name = formElem.name;

			// Not given a name, so just move on.
			if (name === '') {
				continue;
			}

			// Get the value of the input.
			let value: FormValue;
			if (formElem instanceof HTMLInputElement) {
				if (formElem.type === 'checkbox') {
					value = formElem.checked;
				}
				else if (formElem.type === 'radio') {
					if (formElem.checked) {
						value = formElem.value;
					}
					else {
						continue;
					}
				}
				else if (formElem.type === 'file') {
					value = formElem.files;
				}
				else {
					value = formElem.value;
				}
			}
			else if (formElem instanceof HTMLTextAreaElement) {
				value = formElem.value;
			}
			// HTMLSelectElement
			else {
				value = formElem.value;
			}

			// Given the name, put the value into the right spot in values.
			const nameTokens = name.split('.');
			let currentValues = values;
			for (let i = 0, l = nameTokens.length; i < l; i++) {
				let nameToken = nameTokens[i]!;

				// Check if the token is a new array item.
				const isArrayItem = nameToken.endsWith('[]') || nameToken.endsWith('[+]');
				const isNewArrayItem = isArrayItem && nameToken.endsWith('[+]');
				if (isNewArrayItem) {
					nameToken = nameToken.substring(0, nameToken.length - 3);
				}
				else if (isArrayItem) {
					nameToken = nameToken.substring(0, nameToken.length - 2);
				}

				let nextValues = currentValues[nameToken];

				// If we don't yet have anything for this token, make an empty one.
				if (currentValues[nameToken] === undefined) {
					if (isArrayItem) {
						nextValues = [];
						currentValues[nameToken] = nextValues;
					}
					else if (i + 1 < l) {
						nextValues = {};
						currentValues[nameToken] = nextValues;
					}
				}

				// If it's an array, but it wasn't, error.
				if (isArrayItem && !Array.isArray(nextValues)) {
					throw new Error(`In ${name}, ${nameToken} was previously an object, but is an array here.`);
				}

				// If it's not an array, but it was, error.
				if (!isArrayItem && Array.isArray(nextValues)) {
					throw new Error(`In ${name}, ${nameToken} was previously an array, but is an object here.`);
				}

				// If this is not the last token, find the next values to go in.
				if (i + 1 < l) {

					// If it's an array,
					if (Array.isArray(nextValues)) {

						// If it's designated as a new item in an array, make a new item and we'll go into it.
						if (isNewArrayItem || nextValues.length === 0) {
							nextValues.push({});
						}

						// Get the last item of the array.
						nextValues = nextValues[nextValues.length - 1];
					}

					// If the item is a FormValue but we have more to go, error.
					if (typeof nextValues !== 'object' || nextValues === null || nextValues instanceof FileList) {
						throw new Error(`In ${name}, there are more tokens, but the token ${nameToken} is already an leaf value.`);
					}

					// Iterate to the next values.
					currentValues = nextValues;
				}

				// We're at the leaf, assign params.
				else {

					// Nothing new, just assign it.
					if (nextValues === undefined) {
						nextValues = value;
						currentValues[nameToken] = nextValues;
					}

					// If it's an array, push on another value.
					else if (Array.isArray(nextValues)) {
						nextValues.push(value);
					}

					// If the item is not FormValues but we're a leaf, error.
					else if (typeof nextValues === 'object' && nextValues !== null && !(nextValues instanceof FileList)) {
						throw new Error(`In ${name}, there are no more tokens, but the token ${nameToken} is not a leaf value.`);
					}

					// It's a regular FormValue, but it was already set.
					else {
						throw new Error(`In ${name}, the token ${nameToken} is already set.`);
					}
				}
			}
		}

		// Return the values.
		return values;
	}

	// /** Sets the current input values as an object. */
	// static setValues(elem: Element, values: { [key: string]: ValueType }): void {

	// 	for (const entry of Object.entries(values)) {
	// 		const name = entry[0];
	// 		const value = entry[1];

	// 		if (Array.isArray(value)) {
	// 			const formListElem = elem.querySelector(`.FormList[name="${name}"]:not(:scope .FormList .FormList)`);
	// 			if (formListElem === null) {
	// 				throw new Error(`The FormList with name "${name}" must exist.`);
	// 			}
	// 		}
	// 		else {
	// 			const elemFound = elem.querySelector(`[name="${name}"]:not(:scope .FormList [name="${name}"]`);
	// 			if (typeof value === 'boolean') {
	// 				if (!(elemFound instanceof HTMLInputElement) || elemFound.type !== 'checkbox') {
	// 					throw new Error(`The element with name "${name}" must be a checkbox input.`);
	// 				}
	// 				elemFound.checked = value;
	// 			}
	// 			else if (typeof value === 'string') {
	// 				if (!(elemFound instanceof HTMLInputElement) && !(elemFound instanceof HTMLTextAreaElement) && !(elemFound instanceof HTMLSelectElement)) {
	// 					throw new Error(`The element with name "${name}" must be an input, textarea, or select.`);
	// 				}
	// 				if (elemFound instanceof HTMLInputElement && elemFound.type === 'radio') {
	// 					for (const radioElem of elem.querySelectorAll(`input[type="radio"][name="${name}"]:not(:scope .FormList input`) as NodeListOf<HTMLInputElement>) {
	// 						if (radioElem.value === value) {
	// 							radioElem.checked = true;
	// 							break;
	// 						}
	// 					}
	// 				}
	// 				else {
	// 					elemFound.value = value;
	// 				}
	// 			}
	// 			else {
	// 				throw new Error(`The value "${name}" must be a string, boolean, or array of objects.`);
	// 			}
	// 		}
	// 	}
	// 	// Find all first-level FormLists and repeat this call on them.
	// 	const formListElems = elem.querySelectorAll('.FormList:not(:scope .FormList .FormList)');
	// 	for (const formListElem of formListElems) {

	// 		// Get the form list 'name' attribute and use it as the array name.
	// 		const formListName = formListElem.getAttribute('name');
	// 		if (formListName === null) {
	// 			throw new Error('Every FormList must have a "name" attribute in order to get form values from it.');
	// 		}
	// 		if (formListName in values) {
	// 			throw new Error(`The FormList name ${formListName} must only appear once.`);
	// 		}

	// 		// For each item in the form list, run getValues on it and put the result in the corresponding array slot.
	// 		const formListItemElems = Array.from(formListElem.querySelectorAll('.item:not(:scope .FormList .item)'));
	// 		for (let i = 0, l = formListItemElems.length; i < l; i++) {
	// 			const array = values[formListName];
	// 			if (!Array.isArray(array)) {
	// 				throw new Error(`The value ${formListName} must be an array.`);
	// 			}
	// 			const value = array[i];
	// 			if (value !== undefined) {
	// 				this.setValues(formListItemElems[i], value);
	// 			}
	// 		}
	// 	}

	// 	// Go
	// 	const inputElems = elem.querySelectorAll('select:not(:scope .FormList select');
	// 	for (const inputElem of inputElems) {
	// 		const value = values[inputElem.name];
	// 		if (value === undefined) {
	// 			continue;
	// 		}
	// 		if (inputElem.type === 'checkbox') {
	// 			if (typeof value === 'boolean') {
	// 				inputElem.checked = value;
	// 			}
	// 		}
	// 		else if (inputElem.type === 'radio') {
	// 			if (value === inputElem.value) {
	// 				inputElem.checked = true;
	// 			}
	// 			else if (typeof value === 'string') {
	// 				inputElem.checked = false;
	// 			}
	// 		}
	// 		else {
	// 			if (typeof value === 'string') {
	// 				inputElem.value = value;
	// 			}
	// 		}
	// 	}
	// 	const textAreaElems = elem.querySelectorAll('textarea');
	// 	for (const textAreaElem of textAreaElems) {
	// 		const value = values.get(textAreaElem.name);
	// 		if (typeof value === 'string') {
	// 			textAreaElem.value = value;
	// 		}
	// 	}
	// 	const selectElems = elem.querySelectorAll('select');
	// 	for (const selectElem of selectElems) {
	// 		const value = values.get(selectElem.name);
	// 		if (value === undefined) {
	// 			continue;
	// 		}
	// 		if (typeof value === 'string') {
	// 			selectElem.value = value;
	// 		}
	// 	}
	// }

	/** Sets a form as enabled or disabled. */
	static setEnabled(form: Element, enabled: boolean): void {
		if (enabled) {
			form.classList.remove('disabled');
		}
		else {
			form.classList.add('disabled');
		}
		form.querySelectorAll('input, select, textarea, button').forEach((elem) => {
			(elem as HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement | HTMLButtonElement).disabled = !enabled;
		});
	}

	static clear(elem: Element): void {
		const inputElems = elem.querySelectorAll('input');
		for (const inputElem of inputElems) {
			inputElem.value = '';
		}
		const textAreaElems = elem.querySelectorAll('textarea');
		for (const textAreaElem of textAreaElems) {
			textAreaElem.value = '';
		}
		const selectElems = elem.querySelectorAll('select');
		for (const selectElem of selectElems) {
			selectElem.value = '';
		}
	}
}
