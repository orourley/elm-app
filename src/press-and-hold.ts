/** Implements a callback when the user does a press-and-hold. */
export class PressAndHold {

	/** Enables long touch on an element. */
	static enable(elem: Element, pressCallback: (elem: Element) => void, releaseCallback: (elem: Element) => void): Record<'start' | 'move' | 'end' | 'preventDefault', (event: PointerEvent) => void> {

		/** The timeout id used for setting and clearing the timeout. */
		let timeoutId: number | undefined;

		/** The screenX value when the press starts. */
		let xStart: number | undefined;

		/** The screenY value when the press starts. */
		let yStart: number | undefined;

		// Setup the event listener callbacks for the various events.
		function start(event: PointerEvent): void {
			xStart = event.screenX;
			yStart = event.screenY;
			pressCallback(elem);

			timeoutId = window.setTimeout(() => {
				if (timeoutId === undefined || !document.contains(elem)) {
					return;
				}
				timeoutId = undefined;
				releaseCallback(elem);
			}, 500);
		}
		function move(event: PointerEvent): void {
			if (timeoutId === undefined) {
				return;
			}
			if (xStart === undefined || yStart === undefined) {
				return;
			}
			const diffX = event.screenX - xStart;
			const diffY = event.screenY - yStart;
			if (diffX * diffX + diffY * diffY >= 25) {
				end();
			}
		}
		function end(): void {
			if (timeoutId === undefined) {
				return;
			}
			window.clearTimeout(timeoutId);
			timeoutId = undefined;
		}
		function preventDefault(event: Event): void {
			event.preventDefault();
		}

		// Add the event listeners to the various events.
		elem.addEventListener('pointerdown', start as EventListener);
		elem.addEventListener('pointermove', move as EventListener);
		elem.addEventListener('pointerup', end as EventListener);
		elem.addEventListener('contextmenu', preventDefault as EventListener);

		// Return the object for use in the disable method.
		return {
			start,
			move,
			end,
			preventDefault
		};
	}

	/** Disables long touch on an element. */
	static disable(elem: Element, obj: Record<'start' | 'move' | 'end' | 'preventDefault', (event: PointerEvent) => void>): void {
		elem.removeEventListener('pointerdown', obj.start as EventListener);
		elem.removeEventListener('pointermove', obj.move as EventListener);
		elem.removeEventListener('pointerup', obj.end as EventListener);
		elem.removeEventListener('contextmenu', obj.preventDefault as EventListener);
	}
}
