/** Scrolls an element into view of the scrollable container it is in. */
export function scrollIntoView(element: Element, container: Element): void {
	const elementRect = element.getBoundingClientRect();
	const containerRect = container.getBoundingClientRect();
	if (elementRect.bottom > containerRect.bottom) {
		element.scrollIntoView(false);
	}
	if (elementRect.top < containerRect.top) {
		element.scrollIntoView();
	}
}
