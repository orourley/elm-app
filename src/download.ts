import type { JsonType } from '@orourley/pine-lib';

export async function download(url: string, type: 'text'): Promise<string>;
export async function download(url: string, type: 'json'): Promise<JsonType>;
export async function download(url: string, type: 'bytes'): Promise<Uint8Array>;
export async function download(url: string, type: 'float32s'): Promise<Float32Array>;
export async function download(url: string, type: 'text' | 'json' | 'bytes' | 'float32s'): Promise<string | JsonType | Uint8Array | Float32Array> {
	let response: Response;
	try {
		response = await fetch(url);
	}
	catch (e) {
		throw new Error(`Error downloading "${url}": ${e}`);
	}
	if (200 <= response.status && response.status <= 299) {
		if (type === 'text') {
			return response.text();
		}
		else if (type === 'json') {
			return response.json() as Promise<JsonType>;
		}
		else if (type === 'bytes') {
			return new Uint8Array(await response.arrayBuffer());
		}
		else {
			return new Float32Array(await response.arrayBuffer());
		}
	}
	else {
		throw new Error(`Failed download of "${url}". ${response.status} ${response.statusText}.`);
	}
}
