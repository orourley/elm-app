export class CssHelper {
	static getPixelsOfStyle(elem: Element, style: string, pseudo?: string): number {
		const valueString = getComputedStyle(elem, pseudo).getPropertyValue(style);
		if (!valueString.endsWith('px')) {
			throw new Error(`Style ${style} does not have a numerical computed value.`);
		}
		return parseFloat(valueString);
	}
}
