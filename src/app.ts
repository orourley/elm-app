import { Component } from './component';

/** A type for when we add component types to window.UI. */
type WindowWithApp = Window & typeof globalThis & { app: App; };

/** A very simple scaffolding for apps. */
export abstract class App extends Component {

	/** Static block to let the code in here access private members. */
	static {

		// Once the window 'pageshow' event has been triggered, construct the app.
		window.addEventListener('pageshow', () => {
			if (App._AppClass) {
				try {
					void new App._AppClass();
				}
				catch (e) {
					// eslint-disable-next-line no-console
					console.error(e);
				}
			}
			else {
				// eslint-disable-next-line no-console
				console.log('Use YourApp.setAppClass().');
			}
		});

		// Destroy the app on pagehide.
		window.addEventListener('pagehide', () => {
			(window as WindowWithApp).app.destroy();
		});
	}

	/** Constructs the app inside the body. */
	constructor() {
		// Call the super, setting the parent to be undefined.
		super({
			parent: undefined,
			classes: [],
			attributes: new Map(),
			eventHandlers: new Map(),
			innerHtml: '',
			innerHtmlContext: undefined
		});

		(window as WindowWithApp).app = this;
	}

	/** Opens an overlay with the HTML. */
	openOverlay<T extends Component | HTMLElement>(html: string, htmlContext?: Component): T {

		// Make sure the last element of the app is an overlay container.
		let overlays;
		if (!this.queryHas('> .overlays')) {
			overlays = this.insertHtml('<div class="overlays"></div>')[0] as HTMLDivElement;
		}
		else {
			overlays = this.query('> .overlays', Element);
			if (overlays !== this.root.lastChild) {
				this.root.append(overlays);
			}
		}

		// Create the overlay given the html.
		const overlayElems = this.insertHtml(html, overlays, undefined, htmlContext);
		if (overlayElems.length !== 1 || !(overlayElems[0] instanceof HTMLElement)) {
			throw new Error('Invalid overlay html. It needs a single HTMLElement as the root.');
		}
		const overlayElem = overlayElems[0];

		// Return the overlay to the user.
		if (overlayElem.classList.contains('Component')) {
			return this.queryComponent(overlayElems[0], Component) as T;
		}
		else {
			return overlayElem as T;
		}
	}

	/** Closes an overlay. */
	closeOverlay(overlay: Component | HTMLElement | SVGElement): void {
		if (overlay instanceof Component) {
			this.removeComponent(overlay);
		}
		else {
			this.removeNode(overlay);
		}
	}

	/** Sets the subclass of App to be instantiated. It should be called in the main script,
	 *  outside of any function. */
	static setAppClass(): void {
		App._AppClass = this as unknown as new () => App;
	}

	/** The subclass of App to be instantiated. */
	private static _AppClass: (new () => App) | undefined;
}
