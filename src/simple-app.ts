import { App } from './app';
import { Component, type Params } from './component';
import { Router } from './router';

export abstract class SimpleApp extends App {

	/** Gets the parent element of the page and the child element where the page will be inserted before. */
	protected abstract getPageParentAndBefore(): [Element, Element];

	/** Callback when there's a new page. */
	protected abstract onNewPage(page: SimpleApp.Page): void;

	/** Gets the current page name. */
	getPageName(): string {
		return this.pageName;
	}

	/** Starts the router. */
	protected async startRouter(): Promise<void> {

		// Setup the router callback that handles page processing.
		this.router.addCallback(this.processQuery.bind(this));

		// Start it up.
		await this.router.start();
	}

	/** Gets a new router query that combines with the existing router query. */
	getRouterString(newQuery: Router.Query): string {
		return this.router.getRouterString(newQuery);
	}

	/** Pushes a query to the history and process it, calling the callback. \
	 *  All options default to false if omitted.
	 *  * If mergeOldQuery is true, the new query will be merged into the old query, overwriting any key-value pairs.
	 *  * If overwriteHistory is true, the browser history will not add a new entry, but will overwrite the previous entry.
	 *  * If noCallbacks is true, the callbacks will not be called. */
	setRouterQuery(query: Router.Query, options?: { mergeOldQuery?: boolean; overwriteHistory?: boolean; noCallbacks?: boolean; }): void {
		this.router.setQuery(query, options);
	}

	/** Refreshes the query, calling processQuery on the same query. */
	refresh(): void {
		this.router.setQuery({}, { mergeOldQuery: true, overwriteHistory: true });
	}

	/** Registers a component as a page. If pageName is an empty string, it's the default page. */
	protected registerPage(pageName: string, pageType: SimpleApp.PageType): void {
		this.pages.set(pageName, pageType);
		for (const entry of pageType.childPages) {
			const childPageName = entry[0];
			const childPageClass = entry[1];
			this.registerPage(childPageName, childPageClass);
		}
	}

	/** Goes to the specified page. */
	protected goToPage(page: string): void {
		this.setRouterQuery({
			page: page !== '' ? page : undefined
		});
	}

	/** Processes a query, loading a page. Override this to do your own things. */
	protected processQuery(oldQuery: Router.Query | undefined, query: Router.Query): void {

		// If it's the same page, just process the new query parameters.
		if (oldQuery !== undefined && oldQuery['page'] === query['page']) {
			if (this.page !== undefined) {
				this.page.processQuery(oldQuery, query);
			}
			return;
		}

		// Get the new page as a class.
		this.pageName = query['page'] ?? '';
		const PageType = this.pages.get(this.pageName);

		// Invalid page, do nothing, except give a dev console output.
		if (PageType === undefined) {
			throw new Error('Page not found.');
		}

		// Get the page element.
		const [pageParentElement, pageBeforeElement] = this.getPageParentAndBefore();

		// Remove the old page.
		if (this.page !== undefined) {
			this.removeComponent(this.page);
		}

		// Add the new page.
		this.page = this.insertComponent(PageType, pageParentElement, pageBeforeElement, {
			classes: [],
			attributes: new Map(),
			eventHandlers: new Map(),
			innerHtml: '',
			innerHtmlContext: this
		});

		// Call the new page callback.
		this.onNewPage(this.page);

		// Initialize the page, then process the query.
		const page = this.page;
		void this.page.initialize().then(() => {
			// If we're no longer on the same page after initialization, don't do anything.
			if (this.page !== page) {
				return;
			}
			this.page.processQuery(oldQuery, query);
		});
	}

	/** The router system. */
	private router: Router = new Router();

	/** A mapping from page names to page components. */
	private pages = new Map<string, SimpleApp.PageType>();

	/** The current page name. */
	private pageName: string = '';

	/** The current page. */
	private page: SimpleApp.Page | undefined = undefined;
}

export namespace SimpleApp {
	export type PageType = typeof Page & (new (params: Params) => Page);
	export abstract class Page extends Component {

		// Pages to register.
		static childPages = new Map<string, PageType>();

		/** Initializes the page when it first is loaded. */
		async initialize(): Promise<void> {
			return Promise.resolve();
		}

		/** Called after initialization and on every subsequent query change. */
		processQuery(_oldQuery: Router.Query | undefined, _query: Router.Query): void {
		}
	}
}
