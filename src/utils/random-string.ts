/** Helper class for generating random strings. */
export class RandomString {

	/** Generates a random hex string of the given length. */
	static generate(length: number): string {

		// Get a random byte buffer.
		const bytes = new Uint8Array(Math.ceil(length / 2));
		crypto.getRandomValues(bytes);

		// Convert to a hex string.
		// eslint-disable-next-line no-bitwise
		return Array.from(bytes, (byte) => `0${(byte & 0xFF).toString(16)}`.slice(-2)).join('').slice(0, length);
	}
}
