import { JsonHelper, wait, type JsonObject, type JsonType } from '@orourley/pine-lib';
import { SimpleApp } from './simple-app';
import { download } from './download';
import type { Component } from './component';
import type { Router } from './router';
import { WSJson } from './ws-json';

/** The various states that the connection can be. */
export type ConnectionState = 'notConnected' | 'connecting' | 'connectingFailed' | 'notAuthenticated' | 'authenticating' | 'authenticated';

export abstract class FullApp extends SimpleApp {

	/** Changes the display to indicate that the user is logged in. */
	protected abstract onConnectionStateUpdated(prevState: ConnectionState, nextState: ConnectionState): void;

	/** Constructs the app. */
	constructor() {
		super();

		// Register as a websocket handler for receiving messages.
		this._wsJson.registerHandler('users', this.responseHandler.bind(this));

		// Initialize everything else.
		void this.initialize();
	}

	/** INITIALIZE AND DESTROY */

	/** Destroys the app. */
	override destroy(): void {
		this._wsJson.close();
	}

	/** Initializes the asynchronous parts of the app. */
	protected async initialize(): Promise<void> {

		// Load the config.
		await this.loadConfig();

		// Make sure we're connected and authenticated.
		await this.checkConnection();

		// Start the router.
		await this.startRouter();
	}

	/** Check if we're connected before processing the query. */
	protected override processQuery(oldQuery: Router.Query | undefined, query: Router.Query): void {

		// Call super, only if authenticated (logged in) or not authenticated (hopefully at a login page).
		if (this._connectionState === 'authenticated' || this._connectionState === 'notAuthenticated') {
			super.processQuery(oldQuery, query);
		}
	}

	/** ROUTE CONFIRMATION */

	/** CONFIG */

	/** Gets a value from the config. */
	getConfigValue(key: string): string | number | boolean | undefined {
		return this._config.get(key);
	}

	/** Loads the config. */
	private async loadConfig(): Promise<void> {
		try {
			const config = await download('config.json', 'json');
			if (JsonHelper.isObject(config)) {
				for (const key of Object.keys(config)) {
					const value = config[key];
					if (JsonHelper.isString(value) || JsonHelper.isNumber(value) || JsonHelper.isBoolean(value)) {
						this._config.set(key, value);
					}
				}
			}
		}
		catch { /* empty */ }
	}

	/** SERVER CONNECTION */

	/** Sends JSON data to the server and retrieves a JSON response as a specified type. */
	async send<Type extends JsonType | void | undefined>(module: string, command: string, params: JsonObject): Promise<Type> {
		return this._wsJson.send(module, command, params) as Promise<Type>;
	}

	/** Register a handler for receiving unsolicited messages to a module. */
	registerServerHandler(module: string, handler: (command: string, params: JsonObject) => Promise<JsonType | void>): void {
		this._wsJson.registerHandler(module, handler);
	}

	/** Unregister a handler for receiving unsolicited messages to a module. */
	unregisterServerHandler(module: string, handler: (command: string, params: JsonObject) => Promise<JsonType | void>): void {
		this._wsJson.unregisterHandler(module, handler);
	}

	/** Connects to and authenticates with the server. */
	private async checkConnection(): Promise<void> {

		// If we're not connected, try to connect.
		if (this._connectionState === 'notConnected') {

			// Update the connection state.
			this._connectionState = 'connecting';
			this.onConnectionStateUpdated('notConnected', 'connecting');

			await this.connectToServer();
		}

		// If we're connected but not yet authenticated, try to authenticate.
		if (this._connectionState === 'authenticating') {
			await this.authenticate();
		}
	}

	private async connectToServer(): Promise<void> {

		// Get the server URL from the config.
		const serverUrl = this._config.get('serverUrl');
		if (!JsonHelper.isString(serverUrl) || serverUrl === '') {

			// Update the connection state.
			const oldConnectionState = this._connectionState;
			this._connectionState = 'connectingFailed';
			this.onConnectionStateUpdated(oldConnectionState, this._connectionState);

			// Throw the error.
			throw new Error('The config.json must have a "serverUrl" attribute where the web socket can connect to.');
		}

		// Try a few times to connect.
		let failedAttempts = 0;
		while (true) {

			try {
				// Do the connecting.
				await this._wsJson.connect(serverUrl);

				// Update the connection state.
				const oldConnectionState = this._connectionState;
				this._connectionState = 'authenticating';
				this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
				break;
			}
			catch {
				failedAttempts += 1;
				if (failedAttempts === 3) {
					// Connection failed, so update the connection state and give up.
					const oldConnectionState = this._connectionState;
					this._connectionState = 'connectingFailed';
					this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
					break;
				}
				await wait(5);
			}
		}

		// When the connection closes, wait a bit and try to reconnect.
		this._wsJson.setCloseCallback(() => {

			// Update the connection state.
			const oldConnectionState = this._connectionState;
			this._connectionState = 'notConnected';
			this.onConnectionStateUpdated(oldConnectionState, this._connectionState);

			// Wait a bit and then try to reconnect and authenticate.
			void wait(5).then(async () => {
				await this.checkConnection();
			});
		});
	}

	/** When a command is received from the server. */
	private async responseHandler(command: string): Promise<void> {

		// The user logged out somewhere else, so log out here.
		if (command === 'logout') {

			// Unset the local storage.
			this._username = undefined;
			localStorage.removeItem('username');
			localStorage.removeItem('sessionId');

			// Update the connection state.
			if (this._connectionState !== 'notAuthenticated') {
				const oldConnectionState = this._connectionState;
				this._connectionState = 'notAuthenticated';
				this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
			}
		}
	}

	/** LOGIN AND LOGOUT */

	/** Gets the username. */
	getUsername(): string | undefined {
		return this._username;
	}

	/** Gets whether the user is admin. */
	async isAdmin(): Promise<boolean> {
		if (this._connectionState !== 'authenticated') {
			return Promise.resolve(false);
		}
		return this.send<boolean>('users', 'isAdmin', {
			username: this._username
		});
	}

	/** Logs in with a username and password. */
	async login(username: string, password: string): Promise<void> {

		// Clear the local storage.
		localStorage.removeItem('username');
		localStorage.removeItem('sessionId');

		// Try to login and get the session id.
		const sessionId = await this.send<string>('users', 'login', {
			username,
			password
		});

		// Set the local storage.
		localStorage.setItem('username', username);
		localStorage.setItem('sessionId', sessionId);

		// Try to authenticate.
		await this.authenticate();
	}

	/** Logs the user out. */
	async logout(): Promise<void> {

		// Clear the username and local storage.
		this._username = undefined;
		localStorage.removeItem('username');
		localStorage.removeItem('sessionId');

		// Send a logout command to the server.
		// This will clear all sessions from any other logins of the same user.
		await this._wsJson.send('users', 'logout', {});

		// Update the connection state. It may have already been triggered by the responseHandler.
		if (this._connectionState !== 'notAuthenticated') {
			const oldConnectionState = this._connectionState;
			this._connectionState = 'notAuthenticated';
			this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
		}
	}

	/** Authenticate the user. */
	private async authenticate(): Promise<void> {

		// Authenticate the user or go to the login page.
		this._username = localStorage.getItem('username') ?? undefined;
		const sessionId = localStorage.getItem('sessionId') ?? undefined;
		if (this._username === undefined || sessionId === undefined) {

			// Clear the username and local storage.
			this._username = undefined;
			localStorage.removeItem('username');
			localStorage.removeItem('sessionId');

			// Update the connection state.
			const oldConnectionState = this._connectionState;
			this._connectionState = 'notAuthenticated';
			this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
			return;
		}

		// Try to authenticate with the server.
		try {

			// Send the command.
			await this._wsJson.send('users', 'authenticate', {
				username: this._username,
				sessionId
			});
		}
		catch {

			// Clear the username and local storage.
			this._username = undefined;
			localStorage.removeItem('username');
			localStorage.removeItem('sessionId');

			// Update the connection state.
			const oldConnectionState = this._connectionState;
			this._connectionState = 'notAuthenticated';
			this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
			return;
		}

		// Update the connection state.
		const oldConnectionState = this._connectionState;
		this._connectionState = 'authenticated';
		this.onConnectionStateUpdated(oldConnectionState, this._connectionState);
	}

	/** The config. */
	private _config = new Map<string, string | number | boolean>();

	/** The connection state. */
	private _connectionState: ConnectionState = 'notConnected';

	/** The websocket. */
	private _wsJson: WSJson = new WSJson();

	/** The username, once they are logged in. */
	private _username: string | undefined;

	protected static override childComponentTypes: (typeof Component)[] = [
	];
}
