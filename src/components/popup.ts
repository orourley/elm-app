import { Component, type Params } from '../component';

/** A popup that blocks the whole screen. Use the setWindowHtml() function to set the popup HTMl. */
export abstract class Popup extends Component {

	/** Constructs the Popup. */
	constructor(params: Params) {
		super(params);

		// Get the window component.
		this._window = this.query('.window', HTMLDivElement);

		// Make a focus trap for the window.
		this._window.addEventListener('keydown', (event) => {
			if (event.key === 'Tab') {
				if (event.shiftKey) {
					if (document.activeElement === this._firstFocusableElem) {
						this._lastFocusableElem?.focus();
						event.preventDefault();
					}
				}
				else {
					if (document.activeElement === this._lastFocusableElem) {
						this._firstFocusableElem?.focus();
						event.preventDefault();
					}
				}
			}
		});

		// Set the focus on the window root, so that the next tab will be on the first input.
		// Since it isn't yet connected to the document, wait a frame.
		setTimeout(() => {
			this._window.tabIndex = -1;
			this._window.focus();
		}, 0);

		// Look for further changes in the window to update the first and last focusable elements.
		this._mutationObserver = new MutationObserver(this.updateFocusTrap.bind(this));
		this._mutationObserver.observe(this._window, {
			attributes: true,
			characterData: true,
			childList: true,
			subtree: true
		});
	}

	/** Focus on the window. */
	focus(): void {
		this._window.focus();
	}

	/** Gets the window user content. */
	protected getWindow(): HTMLDivElement {
		return this._window;
	}

	/** Makes a focus trap so that the user can't tab outside of the popup. */
	private updateFocusTrap(): void {
		const focusableElems = this._window.querySelectorAll(
			'a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input:not([disabled]):not([type="hidden"]), select:not([disabled])');
		if (focusableElems.length > 0) {
			this._firstFocusableElem = focusableElems[0] as HTMLElement;
			this._lastFocusableElem = focusableElems[focusableElems.length - 1] as HTMLElement;
		}
		else {
			this._firstFocusableElem = undefined;
			this._lastFocusableElem = undefined;
		}
	}

	/** The window that can contain anything. */
	private _window: HTMLDivElement;

	/** The first focusable element for the focus trap. */
	private _firstFocusableElem: HTMLElement | undefined;

	/** The last focusable element for the focus trap. */
	private _lastFocusableElem: HTMLElement | undefined;

	/** A mutation observer which calls the updateFocusTrap on a DOM change. */
	private _mutationObserver: MutationObserver | undefined;

	protected static override html = `
		<div><div class="window overlay"></div>`;

	protected static override css = `
		.Popup {
			position: fixed;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			background: #00000077;
			display: flex;
		}
		.Popup .window {
			margin: auto;
			max-width: 95vw;
			max-height: 95vh;
			outline: none;
			border-radius: .5rem;
			overflow: auto;
			padding: .5rem;
		}`;
}
