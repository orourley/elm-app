import { Component, type Params } from '../component';

export class Icon extends Component {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Set the source from the attributes.
		const alt = params.attributes.get('alt');
		if (alt === undefined) {
			throw new Error('All icons must have alt attribute for accessibility. It can be empty if the image is decorational.');
		}
		this._alt = alt;

		// Set the source from the attributes.
		const srcValue = params.attributes.get('src');
		if (srcValue !== undefined) {
			this._src = srcValue;
		}

		// Set all of the event handlers directly to the input.
		for (const entry of params.eventHandlers) {
			this.root.addEventListener(entry[0], entry[1]);
		}

		// Do the update to set up the svg.
		void this._update();
	}

	/** Gets the alt text. */
	getAlt(): string {
		return this._alt;
	}

	/** Sets the alt text. */
	setAlt(alt: string): void {
		if (this._alt !== alt) {
			this._alt = alt;
			void this._update();
		}
	}

	/** Gets the src url. */
	getSrc(): string {
		return this._src;
	}

	/** Sets the src url. */
	setSrc(src: string): void {
		if (this._src !== src) {
			this._src = src;
			void this._update();
		}
	}

	/** Updates the svg after being set. */
	private async _update(): Promise<void> {

		// Remove the old children.
		const svgElement = this.query('> svg', SVGElement);
		while (svgElement.lastChild !== null) {
			svgElement.removeChild(svgElement.lastChild);
		}

		// If there's no src, we're done.
		if (this._src === '') {
			return;
		}

		// Add the title using the alt.
		if (this._alt !== '') {
			this.insertHtml(`<title>${this._alt}</title>`, svgElement);
		}
		else {
			svgElement.setAttribute('aria-hidden', 'true');
		}

		// Get the src.
		const response = await fetch(this._src);
		if (response.status < 200 || 299 < response.status) {
			throw new Error(`The source ${this._src} returned ${response.status}: ${response.statusText}`);
		}

		// Parse the text into an svg element.
		const text = await response.text();
		const template = document.createElement('template');
		template.innerHTML = text.trim();
		if (template.content.children.length !== 1 || !(template.content.firstElementChild instanceof SVGElement)) {
			throw new Error(`The source ${this._src} is not a valid .svg file.`);
		}
		const svg = template.content.firstElementChild;

		// Copy over or clear the viewbox.
		const viewBoxAttribute = svg.getAttribute('viewBox');
		if (viewBoxAttribute !== null) {
			svgElement.setAttribute('viewBox', viewBoxAttribute);
		}
		else {
			svgElement.removeAttribute('viewBox');
		}

		// Copy over or clear the fill.
		const fill = svg.getAttribute('fill');
		if (fill !== null) {
			svgElement.setAttribute('fill', fill);
		}
		else {
			svgElement.removeAttribute('fill');
		}

		// Copy over or clear the stroke.
		const stroke = svg.getAttribute('stroke');
		if (stroke !== null) {
			svgElement.setAttribute('stroke', stroke);
		}
		else {
			svgElement.removeAttribute('stroke');
		}

		// Copy over the children.
		while (svg.firstChild !== null) {
			svgElement.appendChild(svg.firstChild);
		}
	}

	/** The alt text. */
	private _alt = '';

	/** The source. */
	private _src = '';

	protected static override html = `
		<span><svg></svg></span>
		`;

	protected static override css = `
		.Icon svg {
			display: block;
			width: 100%;
			height: 100%;
		}
		`;
}
