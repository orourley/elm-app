import { Component, type Params } from '../component';

/** Custom user content within a component. */
export class UserContent extends Component {

	/** Gets the root. */
	declare readonly root: HTMLDivElement;

	/** Constructs the object. */
	constructor(params: Params) {
		super(params);

		// Save the change callback.
		this._changeCallback = params.eventHandlers.get('change');

		// Set the inner HTML as the window HTML.
		this.setHtml(params.innerHtml, undefined, params.innerHtmlContext);

		// If there is a change callback, setup a mutation observer.
		if (this._changeCallback) {
			this._mutationObserver = new MutationObserver(this._changeCallback);
			this._mutationObserver.observe(this.root, {
				attributes: true,
				characterData: true,
				childList: true,
				subtree: true
			});
		}
	}

	/** Destroys the object. */
	protected override destroy(): void {
		if (this._changeCallback) {
			this._mutationObserver!.disconnect();
		}
	}

	/** Gets an element by a query. If scope is undefined, the root is used. */
	override query<Type extends Element>(query: string, type: abstract new (...args: unknown[]) => Type, scope: Element = this.root): Type {
		return super.query(query, type, scope);
	}

	/** Returns true if the query exists. If scope is undefined, the root is used. */
	override queryHas(query: string, scope: Element = this.root): boolean {
		return super.queryHas(query, scope);
	}

	/** Gets all elements by a query. If scope is undefined, the root is used. */
	override queryAll<Type extends Element>(query: string, type: abstract new (...args: unknown[]) => Type, scope: Element = this.root): Type[] {
		return super.queryAll(query, type, scope);
	}

	/** Gets a component by a query. If scope is undefined, the root is used. */
	override queryComponent<Type extends Component>(query: string | Element, type: abstract new (params: Params) => Type, scope: Element = this.root): Type {
		return super.queryComponent(query, type, scope);
	}

	/** Returns true if the component with the query exists. If scope is undefined, the root is used. */
	override queryHasComponent(query: string | Element, type: abstract new (params: Params) => Component, scope: Element = this.root): boolean {
		return super.queryHasComponent(query, type, scope);
	}

	/** Gets all components by a query. If scope is undefined, the root is used. */
	override queryAllComponents<Type extends Component>(query: string, type: abstract new (params: Params) => Type, scope: Element = this.root): Type[] {
		return super.queryAllComponents(query, type, scope);
	}

	/** Sets the inner html for an element. If element is undefined, the root is used.
	 * Cleans up tabs and newlines in the HTML.
	 * Cleans up old ids, handlers, and components and adds new ids, handlers, and components. */
	override setHtml(html: string, element: Element = this.root, context?: Component): void {
		super.setHtml(html, element, context);
	}

	/** Inserts html at the end of the parent or before a child node. If parent is undefined, the root is used.
	 * Returns the nodes of the inserted HTML. */
	override insertHtml(html: string, parent: Node = this.root, before?: Node | Component, context?: Component): Node[] {
		return super.insertHtml(html, parent, before, context);
	}

	/** Inserts a node at the end of the parent or before a child node. If parent is undefined, the root is used.
	 *  Returns the node, or its replacement if a component was created. */
	override insertNode(node: Node, parent: Node = this.root, before?: Node | Component, context?: Component): Node {
		return super.insertNode(node, parent, before, context);
	}

	/** Removes a node. */
	override removeNode(node: Node): void {
		super.removeNode(node);
	}

	/** Clears all children of a node. If node is undefined, the root is used. */
	override clearNode(node: Node = this.root): void {
		super.clearNode(node);
	}

	/** Sets a new component of type *ComponentType* as a child of *parentNode* right before
	 *  the child *beforeChild* using the *params*. If params.parentNode is undefined, the root is used. */
	override insertComponent<T extends Component>(ComponentType: new (params: Params) => T, parentNode: Node = this.root,
		before?: Node | Component, params?: Omit<Partial<Params>, 'parent'>): T {
		return super.insertComponent(ComponentType, parentNode, before, params);
	}

	/** Removes a component from this. */
	override removeComponent(component: Component): void {
		super.removeComponent(component);
	}

	/** Whenever there is a change, this is called. */
	private _changeCallback: (() => void) | undefined;

	/** A mutation observer which calls the changeCallback on a DOM change. */
	private _mutationObserver: MutationObserver | undefined;

	protected static override html = '<div></div>';
}
