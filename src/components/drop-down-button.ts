import { Component, type Params } from '../component';
import { UserContent } from './user-content';

export class DropDownButton extends Component {

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Bind the callbacks.
		this.checkClick = this.checkClick.bind(this);

		// Set the elems for ease of use.
		this.buttonElem = this.query('> .button', HTMLButtonElement);
		this.dropDownElem = this.queryComponent('> .dropDown', UserContent);

		// Set the button HTML.
		if (params.attributes.has('buttontext')) {
			this.setHtml(params.attributes.get('buttontext')!, this.buttonElem, undefined);
		}
		else if (params.attributes.has('buttoniconurl')) {
			this.setHtml(`<icon src="${params.attributes.get('buttoniconurl')!}" alt="Toggle Menu"></icon>`, this.buttonElem, undefined);
		}

		// Set the menu HTML.
		this.dropDownElem.setHtml(params.innerHtml, undefined, params.innerHtmlContext);
	}

	/** Gets the drop down content. */
	getDropDownContent(): UserContent {
		return this.dropDownElem;
	}

	/** Sets the button HTML. */
	setButtonHtml(html: string, htmlContext?: Component): void {
		this.setHtml(html, this.buttonElem, htmlContext);
	}

	/** Opens the drop down. */
	setOpen(open: boolean | 'toggle'): void {
		const wasClosed = this.dropDownElem.root.classList.contains('hidden');
		this.dropDownElem.root.classList.toggle('hidden', typeof open === 'boolean' ? !open : undefined);

		// If it wasn't open and now is, set a click handler for when it loses focus.
		if (wasClosed && !this.dropDownElem.root.classList.contains('hidden')) {
			window.addEventListener('click', this.checkClick);
		}
	}

	/** Returns true if the drop down is open. */
	isOpen(): boolean {
		return !this.dropDownElem.root.classList.contains('hidden');
	}

	/** When the drop down is blurred. */
	checkClick(event: Event): void {
		if (!this.root.contains(event.target as Node)) {
			window.removeEventListener('click', this.checkClick);
			this.dropDownElem.root.classList.add('hidden');
		}
	}

	/** The button element. */
	private buttonElem: HTMLButtonElement;

	/** The menu element. */
	private dropDownElem: UserContent;

	protected static override html = /* html */`
		<div>
			<button class="button icon" onclick="setOpen|toggle"></button>
			<UserContent class="dropDown hidden"></UserContent>
		</div>`;

	protected static override css = /* css */`
		.DropDownButton .dropDown {
			position: absolute;
			overflow-y: auto;
		}`;

	protected static override childComponentTypes: (typeof Component)[] = [
		UserContent
	];
}
