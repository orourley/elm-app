import { Component, type Params } from '../component';
import { CssHelper } from '../css-helper';
import { UserContent } from './user-content';

export class DragList extends Component {

	/** The grabbed handler. */
	onGrabbed: ((item: UserContent, beforeItem: UserContent | undefined, cursorPosRelViewport: [number, number]) => void) | undefined;

	/** The dragged handler. */
	onDragged: ((item: UserContent, beforeItem: UserContent | undefined, cursorPosRelViewport: [number, number]) => void) | undefined;

	/** The released handler. */
	onReleased: ((item: UserContent, beforeItem: UserContent | undefined, cursorPosRelViewport: [number, number]) => void) | undefined;

	/** Constructs the object. */
	constructor(params: Params) {
		super(params);

		// Get whether or not the grab button is visible.
		this._grabButtonVisible = params.attributes.get('nograb') === undefined;

		// Get the event callbacks.
		this.onGrabbed = params.eventHandlers.get('grabbed');
		this.onDragged = params.eventHandlers.get('dragged');
		this.onReleased = params.eventHandlers.get('released');

		// Bind the functions for the future calls.
		this._onGrab = this._onGrab.bind(this);
		this._onDrag = this._onDrag.bind(this);
		this._onRelease = this._onRelease.bind(this);
		this._updateDrag = this._updateDrag.bind(this);
	}

	protected override destroy(): void {
		// Remove the scroll interval.
		window.clearInterval(this._updateDragInterval);
		// Clean up the event listeners.
		window.removeEventListener('mousemove', this._onDrag);
		window.removeEventListener('touchmove', this._onDrag);
		window.removeEventListener('mouseup', this._onRelease);
		window.removeEventListener('touchend', this._onRelease);
	}

	/** Gets whether or not the grab buttons are visible. */
	getGrabButtonVisible(): boolean {
		return this._grabButtonVisible;
	}

	/** Sets whether or not the grab buttons are visible. */
	setGrabButtonVisible(grabButtonVisible: boolean): void {
		this._grabButtonVisible = grabButtonVisible;
		for (const item of this.queryAllComponents('> li > .UserContent', UserContent)) {
			const grabElems = item.queryAll('.grab', Element);
			for (const grabElem of grabElems) {
				if (grabElem instanceof HTMLElement) {
					grabElem.classList.toggle('hidden', !this._grabButtonVisible);
				}
			}
		}
	}

	/** Inserts an item before another. It returns the user content of the item. */
	insertItem(html: string, beforeItem: UserContent | undefined, context: Component | undefined): UserContent {

		// Get the beforeItemLi.
		const beforeItemLi = beforeItem !== undefined ? beforeItem.root.parentElement! : undefined;
		if (beforeItemLi && beforeItemLi.parentElement !== this.root) {
			throw new Error('The param beforeItem is not valid.');
		}

		// Insert the item.
		const itemHtml = `<li class="item"><UserContent>${html}</UserContent></li>`;
		const itemLi = this.insertHtml(itemHtml, this.root, beforeItemLi, context)[0] as HTMLLIElement;

		// Set up all grab elements.
		const itemUserContent = this.queryComponent('> .UserContent', UserContent, itemLi);
		const grabElems = itemUserContent.queryAll('.grab', Element);
		for (const grabElem of grabElems) {
			grabElem.addEventListener('mousedown', this._onGrab);
			grabElem.addEventListener('touchstart', this._onGrab);
			grabElem.setAttribute('tabIndex', '-1');
			if (!this._grabButtonVisible) {
				grabElem.classList.add('hidden');
			}
		}

		// Return the item.
		return itemUserContent;
	}

	/** Removes an item. */
	removeItem(item: UserContent): void {
		const itemLi = item.root.parentElement;
		if (itemLi?.parentElement !== this.root) {
			throw new Error('The param item is not valid.');
		}
		this.removeNode(itemLi);
	}

	/** Moves an item to before the beforeItem. */
	moveItem(item: UserContent, beforeItem: UserContent | undefined): void {

		// Get the item and beforeItem.
		const itemLi = item.root.parentElement;
		if (itemLi?.parentElement !== this.root) {
			throw new Error('The param item is not valid.');
		}
		const beforeItemLi = beforeItem !== undefined ? beforeItem.root.parentElement! : undefined;
		if (beforeItemLi && beforeItemLi.parentElement !== this.root) {
			throw new Error('The param beforeItem is not valid.');
		}

		// Do the move.
		if (beforeItemLi) {
			beforeItemLi.before(itemLi);
		}
		else {
			this.root.append(itemLi);
		}
	}

	/** Gets the number of items. */
	getNumItems(): number {
		return this.root.children.length;
	}

	/** Gets the first item. */
	getFirstItem(): UserContent | undefined {
		if (this.root.firstElementChild) {
			return this._getItemFromLi(this.root.firstElementChild as HTMLLIElement);
		}
		return undefined;
	}

	/** Gets the last item. */
	getLastItem(): UserContent | undefined {
		if (this.root.lastElementChild) {
			return this._getItemFromLi(this.root.lastElementChild as HTMLLIElement);
		}
		return undefined;
	}

	/** Gets the previous item. */
	getPrevItem(item: UserContent): UserContent | undefined {
		const itemLi = item.root.parentElement;
		if (itemLi?.parentElement !== this.root) {
			throw new Error('The param item is not valid.');
		}
		if (itemLi.previousElementSibling) {
			return this._getItemFromLi(itemLi.previousElementSibling as HTMLLIElement);
		}
		return undefined;
	}

	/** Gets the next item. */
	getNextItem(item: UserContent): UserContent | undefined {
		const itemLi = item.root.parentElement;
		if (itemLi?.parentElement !== this.root) {
			throw new Error('The param item is not valid.');
		}
		if (itemLi.nextElementSibling) {
			return this._getItemFromLi(itemLi.nextElementSibling as HTMLLIElement);
		}
		return undefined;
	}

	/** Gets the item element by index, or undefined if not found. */
	getItemContaining(elem: Node): UserContent | undefined {
		while (true) {
			const parent = elem.parentNode;
			if (parent === null) {
				return undefined;
			}
			if (parent === this.root) {
				return this.queryComponent('> .UserContent', UserContent, elem as Element);
			}
			elem = parent;
		}
	}

	/** When the dragger has started dragging. */
	private _onGrab(event: Event): void {
		// If the event isn't cancelable, something else is happening so don't initiate a drag.
		if (!event.cancelable) {
			return;
		}
		// Get the item associated with the grab element.
		let draggedItemElem = (event.target as Element).parentElement ?? undefined;
		while (draggedItemElem !== undefined && draggedItemElem.parentElement !== this.root) {
			draggedItemElem = draggedItemElem.parentElement ?? undefined;
		}
		if (draggedItemElem === undefined) {
			return;
		}
		this._draggedItemLi = draggedItemElem as HTMLLIElement;
		// Get the root top relative to the viewport for use as the origin.
		const rootBounds = this.root.getBoundingClientRect();
		// Record the mouse/touch position of the cursor relative to the root top and set it as the reference.
		this._cursorPosRelViewport = this._getPosFromEvent(event);
		this._cursorGrabPosRelRoot = [this._cursorPosRelViewport[0] - rootBounds.left, this._cursorPosRelViewport[1] - rootBounds.top];
		// Make the dragged item have absolute positioning.
		this._draggedItemLi.style.position = 'relative';
		this._draggedItemLi.style.top = '0px';
		this._draggedItemLi.style.zIndex = '1';
		// Record the height of the dragged item.
		this._draggedItemHeight = this._getItemHeight(this._draggedItemLi);
		// Adjust the paddings of the items.
		this._adjustMargins(false, true);
		// Find the most descendent scrollable element for use in scrolling to keep the elem in view.
		this._scrollableElem = this.root;
		while (true) {
			if (this._scrollableElem === document.body) {
				break;
			}
			const overflowY = getComputedStyle(this._scrollableElem).overflowY;
			if (overflowY === 'scroll' || overflowY === 'auto') {
				break;
			}
			this._scrollableElem = this._scrollableElem.parentElement!;
		}
		this._updateDragInterval = window.setInterval(this._updateDrag, 50);
		// Enable the drag callbacks.
		window.addEventListener('mousemove', this._onDrag);
		window.addEventListener('touchmove', this._onDrag);
		window.addEventListener('mouseup', this._onRelease);
		window.addEventListener('touchend', this._onRelease);
		// Call the callback.
		if (this.onGrabbed) {
			const draggedItem = this._getItemFromLi(this._draggedItemLi);
			const beforeItem = this._beforeItemLi ? this._getItemFromLi(this._beforeItemLi) : undefined;
			this.onGrabbed(draggedItem, beforeItem, this._cursorPosRelViewport);
		}
		event.preventDefault();
	}

	/** When the dragger is moved. */
	private _onDrag(event: Event): void {
		// Get the position of the cursor.
		this._cursorPosRelViewport = this._getPosFromEvent(event);
		// Update the dragging.
		this._updateDrag();
	}

	/** When the dragger is released. */
	private _onRelease(event: Event): void {
		// Remove the scroll interval.
		window.clearInterval(this._updateDragInterval);
		// Clean up the event listeners.
		window.removeEventListener('mousemove', this._onDrag);
		window.removeEventListener('touchmove', this._onDrag);
		window.removeEventListener('mouseup', this._onRelease);
		window.removeEventListener('touchend', this._onRelease);
		if (!this._draggedItemLi) {
			return;
		}
		// See if the order changed at all by checking if the next siblings are the same.
		const draggedItemLi = this._draggedItemLi;
		// Revert the paddings of the items to their original.
		this._adjustMargins(false, true);
		// Place the item back.
		this._draggedItemLi.style.position = '';
		this._draggedItemLi.style.top = '';
		this._draggedItemLi.style.zIndex = '';
		this._draggedItemLi.style.marginTop = '';
		this._draggedItemLi.style.marginBottom = '';
		this._draggedItemLi.style.transition = '';
		if (this._draggedItemLi.nextElementSibling instanceof HTMLLIElement) {
			this._draggedItemLi.nextElementSibling.style.marginTop = '';
			this._draggedItemLi.nextElementSibling.style.transition = '';
		}
		this._draggedItemLi = undefined;
		if (this._beforeItemLi) {
			this._beforeItemLi.style.marginTop = '';
			this._beforeItemLi.style.marginBottom = '';
			this._beforeItemLi.style.transition = '';
		}
		else {
			const lastItemLi = this.getLastItem()?.root.parentElement;
			if (lastItemLi) {
				lastItemLi.style.marginBottom = '';
				lastItemLi.style.transition = '';
			}
		}
		this.root.insertBefore(draggedItemLi, this._beforeItemLi ?? null);
		// Trigger the after released event.
		if (this.onReleased) {
			const draggedItem = this._getItemFromLi(draggedItemLi);
			const beforeItem = this._beforeItemLi ? this._getItemFromLi(this._beforeItemLi) : undefined;
			this.onReleased(draggedItem, beforeItem, this._cursorPosRelViewport);
		}
		if (event.cancelable) {
			event.preventDefault();
		}
	}

	/** Gets the position of the event relative to the viewport. */
	private _getPosFromEvent(event: Event): [number, number] {
		if (event instanceof MouseEvent) {
			return [event.clientX, event.clientY];
		}
		else if (event instanceof TouchEvent) {
			if (event.touches.length > 0) {
				return [event.touches[0]!.clientX, event.touches[0]!.clientY];
			}
		}
		return [NaN, NaN];
	}

	/** Gets the item user content from the li elem. */
	private _getItemFromLi(itemLi: HTMLLIElement): UserContent {
		return this.queryComponent('> .UserContent', UserContent, itemLi);
	}

	/** Gets the height of an item, including margins. */
	private _getItemHeight(itemLi: HTMLLIElement): number {
		const marginTop = CssHelper.getPixelsOfStyle(itemLi, 'margin-top');
		const marginBottom = CssHelper.getPixelsOfStyle(itemLi, 'margin-bottom');
		return itemLi.getBoundingClientRect().height + marginTop + marginBottom;
	}

	/** Updates the scroll so that the item stays in view. */
	private _updateDrag(): void {
		// If the cursor is below or above the scrollableElem, adjust its scrolling.
		if (this._scrollableElem !== undefined) {
			// Get the scrollable elem bounds relative to the viewport.
			const scrollableBounds = this._scrollableElem.getBoundingClientRect();
			// Scroll down if needed.
			const bottomDiff = this._cursorPosRelViewport[1] - scrollableBounds.bottom + 16;
			if (bottomDiff > 0) {
				const scrollChange = Math.round(bottomDiff / 1.5);
				this._scrollableElem.scrollTop += scrollChange;
			}
			// Scroll up if needed.
			const topDiff = this._cursorPosRelViewport[1] - scrollableBounds.top - 16;
			if (topDiff < 0) {
				const scrollChange = Math.max(Math.round(topDiff / 1.5), -this._scrollableElem.scrollTop);
				this._scrollableElem.scrollTop += scrollChange;
			}
		}
		// Get the root top relative to the viewport for use as the origin.
		const rootTopRelViewport = this.root.getBoundingClientRect().top;
		// Set the dragged elem's position.
		this._draggedItemLi!.style.top = `${this._cursorPosRelViewport[1] - (rootTopRelViewport + this._cursorGrabPosRelRoot[1])}px`;
		// Adjust the padding of the other items.
		this._adjustMargins(true, false);
		// Trigger the after drag event.
		if (this.onDragged) {
			const draggedItem = this._getItemFromLi(this._draggedItemLi!);
			const beforeItem = this._beforeItemLi ? this._getItemFromLi(this._beforeItemLi) : undefined;
			this.onDragged(draggedItem, beforeItem, this._cursorPosRelViewport);
		}
	}

	/** Adjusts the margins of the items so that there is gap where the dragged item would be placed. */
	private _adjustMargins(transition: boolean, forceUpdate: boolean): void {
		// Find the item that the dragged item is before.
		let beforeItemLi: HTMLLIElement | undefined;
		for (let i = this.root.children.length - 1; i >= 0; i--) {
			// Get the bounds of the actual list item.
			const child = this.root.children[i] as HTMLLIElement;
			if (child === this._draggedItemLi) {
				continue;
			}
			const prevChild = child.previousElementSibling === this._draggedItemLi
				? child.previousElementSibling.previousElementSibling
				: child.previousElementSibling;
			const prevBounds = prevChild?.getBoundingClientRect();
			const bounds = child.getBoundingClientRect();
			// Compare the top values and choose the before item based on where the cursor is.
			if (prevBounds !== undefined && this._cursorPosRelViewport[1] < prevBounds.top + prevBounds.height / 2) {
				continue;
			}
			if (this._cursorPosRelViewport[1] < bounds.top + bounds.height / 2) {
				beforeItemLi = child;
			}
			else {
				beforeItemLi = undefined;
			}
			break;
		}

		// If it is the same or nothing is being dragged, do nothing.
		if (beforeItemLi === this._beforeItemLi && !forceUpdate) {
			return;
		}
		if (this._draggedItemLi === undefined) {
			return;
		}

		// Restore the increased padding item to its original style.
		if (this._beforeItemLi !== undefined) {
			if (this._draggedItemLi.compareDocumentPosition(this._beforeItemLi) === Node.DOCUMENT_POSITION_PRECEDING) {
				this._draggedItemLi.style.marginTop = '';
				this._draggedItemLi.style.marginBottom = '';
				this._draggedItemLi.style.transition = transition ? 'margin .25s linear' : '';
			}
			if (this._beforeItemLi === this._draggedItemLi.nextElementSibling) {
				this._beforeItemLi.style.marginTop = `-${this._draggedItemHeight}px`;
			}
			else {
				this._beforeItemLi.style.marginTop = '';
			}
			this._beforeItemLi.style.transition = transition ? 'margin .25s linear' : '';
		}
		else {
			const lastItemLi = this.getLastItem()?.root.parentElement;
			if (lastItemLi) {
				lastItemLi.style.marginBottom = '';
				lastItemLi.style.transition = transition ? 'margin .25s linear' : '';
			}
		}

		// Make the new increased padding item have an increased padding.
		this._beforeItemLi = beforeItemLi;

		// Set the margin for the new beforeItem.
		if (this._beforeItemLi !== undefined) {
			if (this._draggedItemLi.compareDocumentPosition(this._beforeItemLi) === Node.DOCUMENT_POSITION_PRECEDING) {
				const existingMarginTop = CssHelper.getPixelsOfStyle(this._beforeItemLi, 'margin-top');
				this._draggedItemLi.style.marginTop = `${existingMarginTop - this._draggedItemHeight}px`;
				if (this._draggedItemLi !== this.root.lastElementChild) {
					const existingMarginBottom = CssHelper.getPixelsOfStyle(this._beforeItemLi, 'margin-bottom');
					this._draggedItemLi.style.marginBottom = `${existingMarginBottom + this._draggedItemHeight}px`;
				}
				this._draggedItemLi.style.transition = transition ? 'margin .25s linear' : '';
			}
			if (this._beforeItemLi === this._draggedItemLi.nextElementSibling) {
				this._beforeItemLi.style.marginTop = '';
			}
			else {
				const existingMarginTop = CssHelper.getPixelsOfStyle(this._beforeItemLi, 'margin-top');
				this._beforeItemLi.style.marginTop = `${existingMarginTop + this._draggedItemHeight}px`;
			}
			this._beforeItemLi.style.transition = transition ? 'margin .25s linear' : '';
		}
		else {
			const lastItemLi = this.getLastItem()?.root.parentElement;
			if (lastItemLi && lastItemLi !== this._draggedItemLi) {
				const existingMarginBottom = CssHelper.getPixelsOfStyle(lastItemLi, 'margin-bottom');
				lastItemLi.style.marginBottom = `${existingMarginBottom + this._draggedItemHeight}px`;
				lastItemLi.style.transition = transition ? 'margin .25s linear' : '';
			}
		}
	}

	/** A flag whether or not the grab button is visible. */
	private _grabButtonVisible: boolean;

	/** The item currently being dragged. */
	private _draggedItemLi: HTMLLIElement | undefined;

	/** The height of the dragged item. */
	private _draggedItemHeight: number = 0;

	/** The item with increased padding that the dragged item, if released, will be placed before. */
	private _beforeItemLi: HTMLLIElement | undefined;

	/** The cursor's original value relative to the viewport. */
	private _cursorPosRelViewport: [number, number] = [0, 0];

	/** The cursor's current value relative to the viewport. */
	private _cursorGrabPosRelRoot: [number, number] = [0, 0];

	/** The most descendent scrollable element. */
	private _scrollableElem: Element | undefined;

	/** The interval for adjusting the scroll of the scrollable element. */
	private _updateDragInterval: number | undefined;

	protected static override html = /* html */`
		<ol>
		</ol>
		`;

	protected static override css = /** css */`
		.DragList {
			display: flex;
			flex-direction: column;
		}
		.DragList li {
			list-style-type: none;
		}
		.DragList .grab.hidden {
			display: none;
		}
		`;

	protected static override childComponentTypes: (typeof Component)[] = [
		UserContent
	];
}
