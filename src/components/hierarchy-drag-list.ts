import { wait } from '@orourley/pine-lib';
import { Component, type Params } from '../component';
import { DragList } from './drag-list';
import type { UserContent } from './user-content';

export class HierarchyDragList extends Component {

	/** The item moved handler. */
	onItemsMoved: ((items: UserContent[]) => void) | undefined;

	/** The item level changed hander. */
	onItemLevelChanged: ((item: UserContent) => void) | undefined;

	/** Constructs the object. */
	constructor(params: Params) {
		super(params);

		// Save the drag list.
		this._dragList = this.queryComponent('> .DragList', DragList);

		// Get one rem in px for future use.
		this._oneRemInPx = parseFloat(getComputedStyle(document.body).fontSize);

		// Get whether or not the grab button is visible.
		this._dragList.setGrabButtonVisible(params.attributes.get('nograb') === undefined);

		// Get the event handlers.
		this.onItemsMoved = params.eventHandlers.get('itemsmoved');
		this.onItemLevelChanged = params.eventHandlers.get('itemlevelchanged');
	}

	/** Gets whether or not the grab buttons are visible. */
	getGrabButtonVisible(): boolean {
		return this._dragList.getGrabButtonVisible();
	}

	/** Sets whether or not the grab buttons are visible. */
	setGrabButtonVisible(grabButtonVisible: boolean): void {
		this._dragList.setGrabButtonVisible(grabButtonVisible);
	}

	/** Inserts an item before another. It returns the item index. */
	insertItem(html: string, beforeItem: UserContent | undefined, level: number, context: Component | undefined): UserContent {
		const item = this._dragList.insertItem(html, beforeItem, context);
		// Update the level of the item.
		this._levels.set(item, level);
		item.root.style.marginLeft = `${level}rem`;
		return item;
	}

	/** Removes an item. */
	removeItem(item: UserContent): void {
		this._dragList.removeItem(item);
	}

	/** Gets the number of items. */
	getNumItems(): number {
		return this._dragList.getNumItems();
	}

	/** Gets the first item. */
	getFirstItem(): UserContent | undefined {
		return this._dragList.getFirstItem();
	}

	/** Gets the last item. */
	getLastItem(): UserContent | undefined {
		return this._dragList.getLastItem();
	}

	/** Gets the previous item. */
	getPrevItem(item: UserContent): UserContent | undefined {
		return this._dragList.getPrevItem(item);
	}

	/** Gets the next item. */
	getNextItem(item: UserContent): UserContent | undefined {
		return this._dragList.getNextItem(item);
	}

	/** Gets the item element by index, or undefined if not found. */
	getItemContaining(elem: Node): UserContent | undefined {
		return this._dragList.getItemContaining(elem);
	}

	/** Shifts an item left or right. */
	shiftItem(item: UserContent, direction: 'left' | 'right', callCallback: boolean): void {
		this._shiftItem(item, this._dragList.getPrevItem(item), direction === 'left' ? -1 : +1, callCallback);
	}

	/** Gets the level of an item. */
	getLevel(item: UserContent): number | undefined {
		return this._levels.get(item);
	}

	/** Get all children of an item. */
	getChildItems(item: UserContent): UserContent[] {
		const childItems = [];
		const level = this._levels.get(item)!;
		let nextItem = this._dragList.getNextItem(item);
		while (nextItem !== undefined) {
			// Get the level and check if it is a child.
			const nextLevel = this._levels.get(nextItem)!;
			if (nextLevel <= level) {
				break;
			}
			childItems.push(nextItem);
			nextItem = this._dragList.getNextItem(nextItem);
		}
		return childItems;
	}

	/** Gets the parent of an item. */
	getParentItem(item: UserContent): UserContent | undefined {
		const level = this._levels.get(item);
		if (level === undefined) {
			return undefined;
		}
		let parent = this.getPrevItem(item);
		while (parent !== undefined) {
			if (this._levels.get(parent)! < level) {
				return parent;
			}
			parent = this.getPrevItem(parent);
		}
		return undefined;
	}

	/** Called just after an item is grabbed. */
	protected onItemGrabbed(item: UserContent, beforeItem: UserContent | undefined, cursorPosRelViewport: [number, number]): void {
		// Shrink the "child" items below the elem.
		this._childrenOfDraggedItem = [];
		const level = this._levels.get(item)!;
		let nextItem = beforeItem;
		while (nextItem !== undefined) {
			// Get the level and check if it is a child.
			const nextLevel = this._levels.get(nextItem)!;
			if (nextLevel <= level) {
				break;
			}
			this._childrenOfDraggedItem.push(nextItem);
			nextItem = this._dragList.getNextItem(nextItem);
		}
		// Get the x value of the cursor.
		this._grabbedXRelViewport = cursorPosRelViewport[0];
		// Make the children of the dragged elem shrunk.
		if (this._childrenOfDraggedItem.length > 0) {
			for (const childItem of this._childrenOfDraggedItem) {
				const childBounds = childItem.root.getBoundingClientRect();
				childItem.root.style.height = `${childBounds.height}px`;
				childItem.root.dataset['height'] = `${childBounds.height}px`;
				childItem.root.classList.add('dragged');
				// Add the shrunk class. In a setTimeout to enable transitions.
				setTimeout(() => {
					childItem.root.style.height = '0';
					childItem.root.style.padding = '0';
					childItem.root.style.overflow = 'hidden';
				}, 0);
			}
		}
	}

	/** Called just after a drag. */
	protected onItemDragged(item: UserContent, beforeItem: UserContent | undefined, cursorPosRelViewport: [number, number]): void {
		// Get the previous element of where the element would be placed.
		let prevItem: UserContent | undefined;
		if (beforeItem !== undefined) {
			prevItem = this._dragList.getPrevItem(beforeItem);
		}
		else {
			const lastItem = this._dragList.getLastItem();
			if (lastItem) {
				prevItem = lastItem;
			}
		}
		while (prevItem !== undefined && this._childrenOfDraggedItem && this._childrenOfDraggedItem.includes(prevItem)) {
			prevItem = this._dragList.getPrevItem(prevItem);
		}
		if (prevItem === item) {
			prevItem = this._dragList.getPrevItem(prevItem);
		}
		// Update the left margin of the item for dragging left and right.
		const diffX = cursorPosRelViewport[0] - this._grabbedXRelViewport;
		const diffLevel = Math.trunc(diffX / this._oneRemInPx);
		const level = this._levels.get(item)!;
		let desiredLevel = level + diffLevel;
		desiredLevel = Math.max(0, desiredLevel);
		// Update any restrictions on the element based on the previous element.
		const prevLevel = prevItem !== undefined ? this._levels.get(prevItem)! : -1;
		desiredLevel = Math.min(desiredLevel, prevLevel + 1);
		// Make sure it doesn't move to the left of any next 'sibling' element.
		let nextSiblingItem = beforeItem;
		while (nextSiblingItem !== undefined && this._childrenOfDraggedItem && this._childrenOfDraggedItem.includes(nextSiblingItem)) {
			nextSiblingItem = this._dragList.getNextItem(nextSiblingItem);
		}
		if (nextSiblingItem !== undefined) {
			const nextSiblingLevel = this._levels.get(nextSiblingItem)!;
			desiredLevel = Math.max(desiredLevel, nextSiblingLevel);
		}
		if (level !== desiredLevel) {
			this._shiftItem(item, prevItem, desiredLevel - level, false);
		}
	}

	/** Called just after the drag is released. */
	protected async onItemReleased(item: UserContent, beforeItem: UserContent | undefined): Promise<void> {
		if (!this._childrenOfDraggedItem) {
			return;
		}
		// Add the item to the children so everything is dragged together.
		this._childrenOfDraggedItem.unshift(item);
		// The before elem may be one of the child shrunk elements, so find the first one that's not.
		while (beforeItem !== undefined && this._childrenOfDraggedItem.includes(beforeItem)) {
			beforeItem = this._dragList.getNextItem(beforeItem);
		}
		// Reinsert the shrunk elems to be after the drag element.
		for (const child of this._childrenOfDraggedItem) {
			this._dragList.moveItem(child, beforeItem);
		}
		// Call the callback on each one.
		if (this.onItemsMoved) {
			this.onItemsMoved(this._childrenOfDraggedItem);
		}
		// Remove the shrunk properties. Use timeout so the transition happens.
		await wait(0.1);
		for (const child of this._childrenOfDraggedItem) {
			child.root.style.height = child.root.dataset['height']!;
			child.root.style.padding = '';
			child.root.style.overflow = '';
		}
		// Change height back to auto once the transition is done.
		await wait(0.250);
		for (const child of this._childrenOfDraggedItem) {
			child.root.style.height = '';
		}
		this._childrenOfDraggedItem = undefined;
	}

	/** Shifts an item to the right or left. Returns the actual number of levels changed. */
	private _shiftItem(item: UserContent, prevItem: UserContent | undefined, amount: number, callCallback: boolean): void {
		// Update the left margin of the item for dragging left and right.
		const level = this._levels.get(item)!;
		const prevLevel = prevItem !== undefined ? this._levels.get(prevItem)! : -1;
		// Move to the right if needed.
		const newLevel = Math.max(0, Math.min(level + amount, prevLevel + 1));
		if (newLevel !== level) {
			// Shift the children too.
			const childItems = this._childrenOfDraggedItem ?? this.getChildItems(item);
			for (const childItem of childItems) {
				const newChildLevel = this._levels.get(childItem)! + newLevel - level;
				this._levels.set(childItem, newChildLevel);
				childItem.root.style.marginLeft = `${newChildLevel}rem`;
				if (callCallback && this.onItemLevelChanged) {
					this.onItemLevelChanged(childItem);
				}
			}
			// Update the level of the item.
			this._levels.set(item, newLevel);
			item.root.style.marginLeft = `${newLevel}rem`;
			if (callCallback && this.onItemLevelChanged) {
				this.onItemLevelChanged(item);
			}
		}
		this._grabbedXRelViewport += (newLevel - level) * this._oneRemInPx;
	}

	/** The drag list. */
	private _dragList: DragList;

	/** The list of UserContent levels. */
	private _levels = new WeakMap<UserContent, number>();

	/** The list of items currently shrunk. */
	private _childrenOfDraggedItem: UserContent[] | undefined = undefined;

	/** The initial x-value of the dragged item. */
	private _grabbedXRelViewport: number = 0;

	/** 1rem in pixels for horizontally shifting things. */
	private _oneRemInPx: number = 0;

	protected static override html = /* html */`
		<div>
			<DragList onGrabbed="onItemGrabbed" onDragged="onItemDragged" onReleased="onItemReleased"></DragList>
		</div>
		`;

	protected static override childComponentTypes: (typeof Component)[] = [
		DragList
	];
}
