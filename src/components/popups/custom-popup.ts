import type { Component, Params } from '../../component';
import { Popup } from '../popup';
import { UserContent } from '../user-content';

/** A popup that blocks the whole screen. Use the setWindowHtml() function to set the popup HTMl. */
export class CustomPopup extends Popup {

	/** Constructs it. */
	constructor(params: Params) {
		super(params);

		// Get the user content.
		this.userContent = this.insertComponent(UserContent, this.getWindow(), undefined, {
			innerHtml: params.innerHtml,
			innerHtmlContext: params.innerHtmlContext
		});
	}

	/** Gets the user content. */
	getUserContent(): UserContent {
		return this.userContent;
	}

	/** The user content. */
	private userContent: UserContent;

	protected static override childComponentTypes: (typeof Component)[] = [
		UserContent
	];
}
