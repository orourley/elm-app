import type { Params } from '../../component';
import { Popup } from '../popup';

export class ConfirmPopup extends Popup {

	/** The no callback. */
	onNo: (() => void) | undefined;

	/** The yes callback. */
	onYes: (() => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Get the event handlers.
		this.onNo = params.eventHandlers.get('no');
		this.onYes = params.eventHandlers.get('yes');

		// Set the content of the window.
		this.setHtml(`
			${params.innerHtml}
			<div class="buttons">
				<div class="no"><button class="button" onclick="noChosen">No</button></div>
				<div class="yes"><button class="button" onclick="yesChosen">Yes</button></div>
			</div>
			`, this.getWindow(), this);
	}

	/** When the no button was pressed. */
	protected noChosen(): void {
		this.app.closeOverlay(this);
		if (this.onNo) {
			this.onNo();
		}
	}

	/** When the yes button was pressed. */
	protected yesChosen(): void {
		this.app.closeOverlay(this);
		if (this.onYes) {
			this.onYes();
		}
	}

	protected static override css = `
		.ConfirmPopup .window {
			display: flex;
			flex-direction: column;
			gap: .5rem;
		}
		.ConfirmPopup .buttons {
			display: grid;
			grid-template-columns: 1fr 1fr;
			gap: .5rem;
		}
		.ConfirmPopup .yes {
			text-align: right;
		}`;
}
