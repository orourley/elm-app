import type { Params } from '../../component';
import { Popup } from '../popup';

export class WaitingPopup extends Popup {

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Set the content of the window.
		this.setHtml(params.innerHtml, this.getWindow(), this);
	}
}
