import type { Params } from '../../component';
import { Popup } from '../popup';

export class OkPopup extends Popup {

	/** The ok callback. */
	onOk: (() => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Get the event handlers.
		this.onOk = params.eventHandlers.get('ok');

		// Set the content of the window.
		this.setHtml(`
			${params.innerHtml}
			<div class="buttons">
				<div class="ok"><button class="button" onclick="okChosen">Ok</button></div>
			</div>
			`, this.getWindow(), this);
	}

	/** When the yes button was pressed. */
	protected okChosen(): void {
		this.app.closeOverlay(this);
		if (this.onOk) {
			this.onOk();
		}
	}

	protected static override css = `
		.OkPopup .window {
			display: flex;
			flex-direction: column;
			gap: .5rem;
		}
		.OkPopup .buttons {
			display: grid;
			grid-template-columns: 1fr;
			gap: .5rem;
		}
		.OkPopup .ok {
			text-align: right;
		}`;
}
