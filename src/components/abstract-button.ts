import { Component, type Params } from '../component';

export abstract class AbstractButton extends Component {

	/** The pressed callback. */
	onPressed: (() => void) | undefined;

	/** The released callback. */
	onReleased: (() => void) | undefined;

	/** The hovered callback. */
	onHovered: ((x: number, y: number) => void) | undefined;

	/** The event callback for when the mousedown or touchstart happens. */
	protected abstract _mouseTouchDown(): void;

	/** Constructs the object. */
	constructor(params: Params) {
		super(params);

		// Register the events.
		this.onPressed = params.eventHandlers.get('pressed');
		this.onReleased = params.eventHandlers.get('released');
		this.onHovered = params.eventHandlers.get('hovered');

		// Add the innerHtml to the button.
		this.insertHtml(params.innerHtml, this.root, undefined, this);
	}

	/** The event callback for when the mousemove happens. */
	protected _mouseMove(event: MouseEvent): void {
		this.root.classList.add('hovered');
		const rect = this.root.getBoundingClientRect();
		if (this.onHovered) {
			this.onHovered(event.clientX - rect.left, event.clientY - rect.top);
		}
	}

	/** The event callback for when the mouseout happens. */
	protected _mouseOut(): void {
		this.root.classList.remove('hovered');
		if (this.onHovered) {
			this.onHovered(Number.NaN, Number.NaN);
		}
	}

	/** Returns true if the coordinates are over the button. */
	protected _isOver(x: number, y: number): boolean {
		const rect = this.root.getBoundingClientRect();
		return rect.left <= x && x < rect.right && rect.top <= y && y < rect.bottom;
	}

	protected static override html = /* html */`
		<div onmousedown="_mouseTouchDown" ontouchstart="_mouseTouchDown" onmousemove="_mouseMove" onmouseout="_mouseOut"></div>
		`;

	protected static override css = /* css */`
		.AbstractButton {
			cursor: pointer;
		}
	`;
}
