import type { Params } from '../component';
import { UserContent } from './user-content';

/** A generic drop down. Should only be used with app.openOverlay(). */
export class DropDown extends UserContent {

	/** The constructor. */
	constructor(params: Params) {
		super(params);

		// Get the onClosed event handler.
		this._onClosed = params.eventHandlers.get('closed');

		// Bind this to callbacks.
		this.onWindowPointerDown = this.onWindowPointerDown.bind(this);
	}

	/** Destroys the drop down. */
	protected override destroy(): void {

		// Call the closed callback.
		this._onClosed?.();

		// Stop listening for window pointer down events.
		window.removeEventListener('pointerdown', this.onWindowPointerDown);
	}

	/** Sets the element that the drop down should be relative to. */
	setRelativeTo(elem: Element): void {

		// Set the drop down in the right position.
		const clientWidth = document.documentElement.clientWidth;
		const clientHeight = document.documentElement.clientHeight;
		const remInPx = parseFloat(getComputedStyle(document.documentElement).fontSize);
		const bounds = elem.getBoundingClientRect();

		// Make sure the drop down is within the viewport.
		if (bounds.bottom + 0.25 * remInPx + this.root.clientHeight > clientHeight - 0.25 * remInPx) {
			this.root.style.bottom = `calc(${clientHeight - bounds.top}px + 0.25rem`;
		}
		else {
			this.root.style.top = `calc(${bounds.bottom}px + 0.25rem`;
		}
		if (bounds.left + this.root.clientWidth > clientWidth - 0.25 * remInPx) {
			this.root.style.right = `${clientWidth - bounds.right}px`;
		}
		else {
			this.root.style.left = `${bounds.left}px`;
		}

		// Start listening for window clicks to close the drop down.
		window.addEventListener('pointerdown', this.onWindowPointerDown);
	}

	/** Sets the onClosed handler. */
	setOnClosedHandler(onClosed: (() => void) | undefined) {
		this._onClosed = onClosed;
	}

	/** Close the drop down. */
	close(): void {
		this.app.closeOverlay(this);
		this._onClosed?.();
	}

	/** Only active when the drop down is open. Clicking anywhere but the drop down closes the drop down. */
	private onWindowPointerDown(event: Event): void {
		const target = event.target;
		if (!(target instanceof Node) || !this.root.contains(target)) {
			this.app.closeOverlay(this);
			this._onClosed?.();
		}
	}

	// The onClosed event handler.
	private _onClosed: (() => void) | undefined;

	protected static override css = `
		.DropDown {
			position: absolute;
			box-shadow: 0 0 2rem 2rem rgba(0, 0, 0, 0.25);
		}
		`;
}
