import { Component, type Params } from '../../component';
import { DragList } from '../drag-list';
import { UserContent } from '../user-content';
import { FormBaseInput } from './form-base-input';

export class FormList extends Component {

	/** The changed handler. */
	onChanged: ((item: UserContent) => void) | undefined;

	/** Constructs the component.
	 * Supports attribute nograb to disable dragging of items.
	 * The inner html is what each item will have once added. */
	constructor(params: Params) {
		super(params);

		// Save the drag list.
		this._dragList = this.queryComponent('.DragList', DragList);

		// Set the grab/nograb class.
		if (params.attributes.has('nograb')) {
			this.root.classList.add('noGrab');
			this._dragList.setGrabButtonVisible(false);
		}

		// Save the handler.
		this.onChanged = params.eventHandlers.get('changed');

		// Save the item HTML.
		this._itemHtml = params.innerHtml;
		this._itemHtmlContext = params.innerHtmlContext;
	}

	/** Adds an item. Returns the item added. */
	add(beforeItem?: UserContent): UserContent {
		const item = this._dragList.insertItem(`
				<button class="grab button icon"><Icon src="assets/icons/grab.svg" alt="grab"></Icon></button>
				<UserContent class="content"></UserContent>
				<button class="remove button icon" onclick="onClickRemove"><Icon src="assets/icons/remove.svg" alt="remove"></Icon></button>
			`, beforeItem?.parent as UserContent | undefined, this);
		const itemContent = item.queryComponent('> .content', UserContent);
		itemContent.setHtml(this._itemHtml, undefined, this._itemHtmlContext);
		this.onChanged?.(itemContent);
		// Return the content of the item added.
		return itemContent;
	}

	/** Removes an item. */
	remove(item: UserContent): void {
		this._dragList.removeItem(item.parent as UserContent);
		this.onChanged?.(item);
	}

	/** Gets the number of items. */
	getNumItems(): number {
		return this._dragList.getNumItems();
	}

	/** Gets the first item. */
	getFirstItem(): UserContent | undefined {
		return this._dragList.getFirstItem()?.queryComponent('> .content', UserContent);
	}

	/** Gets the last item. */
	getLastItem(): UserContent | undefined {
		return this._dragList.getLastItem()?.queryComponent('> .content', UserContent);
	}

	/** Gets the previous item. */
	getPrevItem(item: UserContent): UserContent | undefined {
		return this._dragList.getPrevItem(item.parent as UserContent)?.queryComponent('> .content', UserContent);
	}

	/** Gets the next item. */
	getNextItem(item: UserContent): UserContent | undefined {
		return this._dragList.getNextItem(item.parent as UserContent)?.queryComponent('> .content', UserContent);
	}

	/** Gets the item element by index, or undefined if not found. */
	getItemContaining(elem: Node): UserContent | undefined {
		return this._dragList.getItemContaining(elem)?.queryComponent('> .content', UserContent);
	}

	/** When the user clicks the add button. */
	protected onClickAdd(): void {
		const itemContent = this.add();

		// Focus on the first input. It uses a timeout because the new item might not be yet connected to the document.
		const inputs = itemContent.queryAllComponents('.FormBaseInput', FormBaseInput);
		setTimeout(() => {
			if (inputs.length > 0) {
				inputs[0]!.focus();
			}
		}, 0);

		this.onChanged?.(itemContent);
	}

	/** When the user clicks the remove button in an item. */
	protected onClickRemove(event: Event): void {
		const item = this._dragList.getItemContaining(event.target as Element);
		if (item !== undefined) {
			const itemContent = item.queryComponent('> .content', UserContent);
			this.remove(itemContent);
			this.onChanged?.(itemContent);
		}
	}

	/** When the drag list item is released. */
	protected onDragListReleased(item: UserContent): void {
		this.onChanged?.(item.queryComponent('> .content', UserContent));
	}

	/** The drag list. */
	private _dragList: DragList;

	/** The HTML of each item. */
	private _itemHtml: string;

	/** The context of the HTML of each item. */
	private _itemHtmlContext: Component | undefined;

	protected static override html = /* html */`
		<div>
			<DragList onReleased="onDragListReleased">
			</DragList>
			<button class="add button" onclick="onClickAdd">Add</button>
		</div>
		`;

	protected static override css = /* css */`
		.FormList > .DragList > .item > .UserContent {
			display: flex;
			gap: .25rem;
		}
		.FormList > .DragList > .item > .UserContent > :is(.grab, .remove) {
			flex: 0 0 auto;
		}
		.FormList > .DragList > .item > .UserContent > .content {
			flex: 1 1 auto;
		}
		.FormList > .DragList:not(:empty) + button {
			margin-block-start: .25rem;
		}
		`;

	protected static override childComponentTypes = [
		DragList,
		UserContent
	];
}
