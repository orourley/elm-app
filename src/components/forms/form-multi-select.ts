import type { Component, Params } from '../../component';
import { FormBaseInput } from './form-base-input';

/** A generic dropDown selection. */
export class FormMultiSelect extends FormBaseInput<string[]> {

	/** The input element with the name and value. */
	readonly _inputElem: HTMLInputElement;

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Bind callbacks to this.
		this.checkDropDown = this.checkDropDown.bind(this);

		// Get the value and display input elements.
		this._inputElem = this.query('> input.value', HTMLInputElement);
		this._displayInputElem = this.query('> input.display', HTMLInputElement);

		// Set all of the attributes directly to the input.
		for (const entry of params.attributes) {
			if (entry[0] === 'value') {
				this._inputElem.value = entry[1];
				this.updateFromValue();
			}
			else if (entry[0] === 'filter') {
				// Set whether or not to hide selections that don't match the current input.
				this._filter = params.attributes.has('filter');
			}
			else if (entry[0] !== 'id') {
				this._inputElem.setAttribute(entry[0], entry[1]);
			}
		}

		// Set the id.
		const id = params.attributes.get('id');
		if (id !== undefined) {
			this._displayInputElem.id = id;
		}

		// Set the event handlers.
		this._onChanged = params.eventHandlers.get('changed');
		this._onOptionToggled = params.eventHandlers.get('optiontoggled');

		// Set the drop down html.
		this.addDropDownHtml(params.innerHtml, undefined, params.innerHtmlContext);
	}

	protected override destroy(): void {
		window.removeEventListener('click', this.checkDropDown);
		super.destroy();
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._inputElem.name = name;
		}
		else {
			this._inputElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._displayInputElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._displayInputElem.id = id;
			this.makeIdsUnique(this._displayInputElem);
		}
		else {
			this._displayInputElem.removeAttribute('id');
		}
	}

	/** Gets the value. */
	getValue(): string[] {
		return this._inputElem.value.split(';;;');
	}

	/** Sets the value. */
	setValue(value: string[]) {
		this._inputElem.value = value.join(';;;');
		this.updateFromValue();
	}

	/** Focuses the input. */
	focus(): void {
		this._displayInputElem.focus();
	}

	/** Gets all of the options associated with the value. */
	getOptionsWithValue(value: string): HTMLOptionElement[] {
		return [...this.queryAll(`> .dropDown > option[value="${value}"]`, HTMLOptionElement)];
	}

	/** Adds html for the drop down. Returns any generated options. */
	addDropDownHtml(html: string, beforeElem: HTMLElement | undefined, context: Component | undefined): HTMLOptionElement[] {

		// Just insert the html into the options.
		const nodes = this.insertHtml(html, this.query('> .dropDown', Element), beforeElem, context);

		// Process each option element.
		const options = [];
		for (const node of nodes) {
			if (node instanceof HTMLOptionElement) {

				// Get the option value.
				const optionValue = node.getAttribute('value');
				if (optionValue === null) {
					throw new Error('Every option in a FormMultiSelect must have a value.');
				}

				// Add the click event listener.
				node.addEventListener('click', this.onClickOption.bind(this));

				// Add it to the list of options to be returned.
				options.push(node);
			}
		}

		// Update the input value.
		this.updateInputValue();

		// Return all of the nodes that were options.
		return options;
	}

	/** Removes an element in the drop down menu. */
	removeDropDownElement(elem: HTMLElement): void {

		// Check if it is a valid option.
		if (elem.parentElement !== this.query('> .dropDown', Element)) {
			throw new Error('Not a valid option element.');
		}

		// Clear this selection if needed.
		if (elem instanceof HTMLOptionElement && elem.classList.contains('selected')) {
			elem.classList.remove('selected');
			this._onOptionToggled?.(elem, false);
			this._onChanged?.(this);
		}

		// Remove the option.
		this.removeNode(elem);

		// Update the input value.
		this.updateInputValue();
	}

	/** Clears all of the options. */
	clear(): void {
		const options = this.query('> .dropDown', Element);
		options.innerHTML = '';
	}

	/** Toggles, selects or deselects an option. */
	toggleOption(option: HTMLOptionElement, forceTo?: boolean, depthToKeepGoing?: number): void {

		// Check if it is a valid option.
		if (option.parentElement !== this.query('> .dropDown', Element)) {
			throw new Error('Not a valid option element.');
		}

		// If it's not already in the proper state, toggle it.
		if (option.classList.contains('selected') !== forceTo) {

			// Toggle the option.
			option.classList.toggle('selected');

			// Call the callback.
			this._onOptionToggled?.(option, option.classList.contains('selected'));
			this._onChanged?.(this);
		}

		// Toggle hierarchy children, if any.
		if (option.nextElementSibling instanceof HTMLOptionElement) {
			if (depthToKeepGoing === undefined) {
				depthToKeepGoing = parseInt(option.getAttribute('depth') ?? '0') + 1;
			}
			const nextDepth = parseInt(option.nextElementSibling.getAttribute('depth') ?? '0');
			if (depthToKeepGoing <= nextDepth) {
				this.toggleOption(option.nextElementSibling, option.classList.contains('selected'), depthToKeepGoing);
			}
		}

		// Update the value input.
		this.updateInputValue();
	}

	/** Handles the drop down. */
	protected checkDropDown(event: Event | undefined): void {
		const dropDown = this.query('> .dropDown', Element);
		const dropDownOpen = dropDown.classList.contains('open');
		const eventInside = event !== undefined && (dropDown.contains(event.target as Node) || this._displayInputElem.contains(event.target as Node));
		if (!dropDownOpen && eventInside) {
			dropDown.classList.add('open');
			window.addEventListener('click', this.checkDropDown);
			event.stopPropagation();
		}
		else if (dropDownOpen && !eventInside) {
			dropDown.classList.remove('open');
			window.removeEventListener('click', this.checkDropDown);
		}
	}

	/** Called when the input changes, to handle filtering. */
	protected inputChanged(): void {

		// If we're filtering, see if this option needs to be hidden.
		if (this._filter) {

			// Get the current label text.
			const label = this._displayInputElem.value.toLocaleLowerCase();

			// Go through each option,
			const options = this.queryAll('> .dropDown > option', HTMLOptionElement);
			for (let i = 0, l = options.length; i < l; i++) {

				// Get the option, value, and label.
				const option = options[i]!;
				const optionLabel = option.innerHTML.toLocaleLowerCase();

				if (optionLabel.includes(label)) {
					option.classList.remove('filteredOut');

					// Show options higher up in a hierarchy.
					let depth = parseInt(option.getAttribute('depth') ?? '0');
					for (let j = i - 1; j >= 0; j--) {
						const otherOption = options[j]!;
						const otherDepth = parseInt(otherOption.getAttribute('depth') ?? '0');
						if (otherDepth < depth) {
							otherOption.classList.remove('filteredOut');
							depth = otherDepth;
						}
					}
				}
				else {
					option.classList.add('filteredOut');
				}
			}
		}
	}

	/** Update the component when the value has changed. */
	private updateFromValue(): void {

		// Get all of the values as a set.
		const values = this._inputElem.value.split(';;;');
		if (values.length === 1 && values[0] === '') {
			values.length = 0;
		}
		const valueSet = new Set(values);

		// Make each option selected or not, if it has a value.
		const options = this.queryAll('> .dropDown > option', HTMLOptionElement);
		let anyChanged = false;
		for (const option of options) {
			if (option.classList.contains('selected') && !valueSet.has(option.value)) {
				option.classList.remove('selected');
				this._onOptionToggled?.(option, false);
				anyChanged = true;
			}
			else if (!option.classList.contains('selected') && valueSet.has(option.value)) {
				option.classList.add('selected');
				this._onOptionToggled?.(option, true);
				anyChanged = true;
			}
		}
		if (anyChanged) {
			this._onChanged?.(this);
		}
	}

	/** Called when an option is clicked. */
	private onClickOption(event: MouseEvent): void {
		const optionElem = event.target as HTMLOptionElement;
		this.toggleOption(optionElem);
	}

	/** Updates the input value. */
	private updateInputValue(): void {
		const selectedOptions = this.queryAll('> .dropDown > option.selected', HTMLOptionElement);
		const newValue = selectedOptions.map((selectedOption) => selectedOption.value).join(';;;');
		if (this._inputElem.value !== newValue) {
			this._inputElem.value = newValue;
			this.updateFromValue();
		}
	}

	/** Whether or not to hide selections that don't match the current input. */
	private _filter: boolean = false;

	/** The changed callback. */
	private _onChanged: ((input: FormMultiSelect) => void) | undefined;

	/** The option toggled callback. */
	private _onOptionToggled: ((option: HTMLOptionElement, selected: boolean) => void) | undefined;

	/** The input element with the display text. */
	private _displayInputElem: HTMLInputElement;

	protected static override html = /* html */`
		<span>
			<input class="value" type="hidden" onfocus="checkDropDown" onblur="checkDropDown" oninput="inputChanged"></input>
			<input class="display input" onfocus="checkDropDown" onblur="checkDropDown" oninput="inputChanged"></input>
			<div class="dropDown input"></div>
		</span>
		`;

	protected static override css = /* css */`
		.FormMultiSelect {
			display: inline-block;
			position: relative;
		}
		.FormMultiSelect > .display {
			display: inline-block;
			width: 100%;
		}
		.FormMultiSelect > .dropDown {
			width: 100%;
			max-height: 8rem;
			display: none;
			overflow-y: auto;
			margin-top: 0.25rem;
		}
		.FormMultiSelect > .dropDown.open {
			display: block;
		}
		.FormMultiSelect > .dropDown > [value] {
			cursor: pointer;
		}
		.FormMultiSelect > .dropDown > .filteredOut {
			display: none;
		}
		`;
}
