import { Component } from '../../component';

/** A base input for forms. All other inputs should inherit from this for uniformity.
 *  Each input should have the 'changed' event handler of the form (input) => void. */
export abstract class FormBaseInput<GetValue, SetValue = GetValue> extends Component {

	/** Gets the name. */
	abstract getName(): string;

	/** Sets the name. */
	abstract setName(name: string): void;

	/** Gets the id. */
	abstract getId(): string;

	/** Sets the id. */
	abstract setId(id: string): void;

	/** Gets the value. */
	abstract getValue(): GetValue;

	/** Sets the value. */
	abstract setValue(value: SetValue): void;

	/** Focuses the input. */
	abstract focus(): void;
}
