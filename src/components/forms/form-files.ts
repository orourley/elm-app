import type { Params } from '../../component';
import { FormBaseInput } from './form-base-input';
import { FormInput } from './form-input';

export class FormFiles extends FormBaseInput<File[], never> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the input element.
		this._inputElem = this.query('input', HTMLInputElement);

		// Get the name attribute.
		const name = params.attributes.get('name');
		if (name !== undefined) {
			this._inputElem.name = name;
		}
		const id = params.attributes.get('id');
		if (id !== undefined) {
			this._inputElem.id = id;
		}

		// Get the multiple flag.
		this._inputElem.multiple = params.attributes.has('multiple') && params.attributes.get('multiple') !== 'false';

		// Set the event handlers.
		this._onChanged = params.eventHandlers.get('changed');
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		this._inputElem.name = name;
	}

	/** Gets the id. */
	getId(): string {
		return this._inputElem.id;
	}

	/** Sets the id. */
	setId(name: string) {
		this._inputElem.id = name;
	}

	/** Focuses the input. */
	focus(): void {
		this._inputElem.focus();
	}

	/** Gets the value. */
	getValue(): File[] {
		return this._files;
	}

	/** Sets the value. */
	setValue(_value: never) {
		throw new Error('Not implemented.');
	}

	/** Called when files are dragged over the drop zone. */
	protected _onDragOver(event: DragEvent): void {
		event.stopPropagation();
		event.preventDefault();
		if (event.dataTransfer !== null) {
			this.root.classList.add('over');
			event.dataTransfer.dropEffect = 'copy';
		}
	}

	/** Called when files are dragged away from the drop zone. */
	protected _onDragLeave(event: DragEvent): void {
		event.stopPropagation();
		event.preventDefault();
		if (event.dataTransfer !== null) {
			this.root.classList.remove('over');
			event.dataTransfer.dropEffect = 'none';
		}
	}

	/** Called when the files are dropped into the drop zone. */
	protected _onDrop(event: DragEvent): void {
		event.stopPropagation();
		event.preventDefault();
		if (event.dataTransfer !== null) {
			this.query('input', HTMLInputElement).files = event.dataTransfer.files;
			this._updateList();
		}
	}

	/** When files are uploaded via the button. */
	protected _onUpload(): void {
		this._updateList();
	}

	/** Updates the list, calling a callback if needed. */
	private _updateList(): void {
		this.clearNode(this.query('.list', Element));
		const files = this._inputElem.files;
		this._files = [];
		if (files) {
			for (const file of files) {
				// Add the name.
				this.insertHtml(`<p>${file.name}</p>`, this.query('.list', Element), undefined);
				this._files.push(file);
			}
			this._onChanged?.(this);
		}
	}

	/** The input element. */
	private _inputElem: HTMLInputElement;

	/** The file list. */
	private _files: File[] = [];

	/** The files callback. */
	private _onChanged: ((input: FormFiles) => void) | undefined;

	protected static override html = /* html */`
		<div onDragOver="_onDragOver" onDragLeave="_onDragLeave" onDrop="_onDrop">
			<p>Drag Files Here</p>
			<p>Or</p>
			<p><label class="button"><span>Upload</span><input class="button" type="file" onInput="_onUpload"></input></label></p>
			<div class="list"></div>
		</div>
		`;

	protected static override css = /* css */`
		.FormFiles > p,
		.FormFiles > div {
			text-align: center;
		}
		.FormFiles label {
			position: relative;
		}
		.FormFiles input {
			position: absolute;
			width: .1rem;
			height: .1rem;
			opacity: 0;
			cursor: pointer;
		}
		`;

	protected static override childComponentTypes = [
		FormInput
	];
}
