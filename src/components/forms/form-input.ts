import type { Params } from '../../component';
import { FormBaseInput } from './form-base-input';

export class FormInput extends FormBaseInput<string> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the element.
		this._inputElem = this.root as HTMLInputElement;

		// Set all of the attributes directly to the input.
		for (const entry of params.attributes) {
			if (entry[0] === 'value') {
				this._inputElem.value = entry[1];
			}
			else if (entry[0] === 'pattern') {
				this._validPattern = new RegExp(String.raw`^(?:${entry[1]})$`, 'v');
			}
			else if (entry[0] === 'class' && entry[1] !== '') {
				this._inputElem.classList.add(...entry[1].split(/\s+/u));
			}
			else {
				this._inputElem.setAttribute(entry[0], entry[1]);
			}
		}

		// Set the event handlers.
		this._onChanged = params.eventHandlers.get('changed');

		// Sets an event listener for unfocusing to check the validity.
		if (this._validPattern !== undefined) {
			this.setBlurOccurred = this.setBlurOccurred.bind(this);
			this._inputElem.addEventListener('blur', this.setBlurOccurred);
		}
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._inputElem.name = name;
		}
		else {
			this._inputElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._inputElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._inputElem.id = id;
			this.makeIdsUnique(this._inputElem);
		}
		else {
			this._inputElem.removeAttribute('id');
		}
	}

	/** Gets the value. */
	getValue(): string {
		return this._inputElem.value;
	}

	/** Sets the value. */
	setValue(value: string) {
		this._inputElem.value = value;
	}

	/** Focuses the input. */
	focus(): void {
		this._inputElem.focus();
	}

	/** When the checkbox changes its input. */
	protected onInput(): void {
		this._onChanged?.(this);
	}

	/** Sets the a blur has occurred. This makes it so that the validity is check on every input change. */
	private setBlurOccurred(): void {
		this.checkValidity();
		this._inputElem.removeEventListener('blur', this.setBlurOccurred);
		this._inputElem.addEventListener('input', this.checkValidity.bind(this));
	}

	/** Checks the validity of the pattern. */
	private checkValidity(): void {
		const value = this._inputElem.value;
		const invalid = this._validPattern && value.match(this._validPattern) === null;
		this.root.classList.toggle('invalid', invalid);
	}

	/** The input element. */
	private _inputElem: HTMLInputElement;

	/** The changed callback. */
	private _onChanged?: ((input: FormInput) => void) | undefined;

	/** The valid pattern. */
	private _validPattern: RegExp | undefined;

	protected static override html = /* html */`
		<input class="input" onInput="onInput" />`;

	protected static override css = /* css */`
		.FormInput[type='number'] {
			appearance: textfield;
		}
		.FormInput[type=number]::-webkit-inner-spin-button,
		.FormInput[type=number]::-webkit-outer-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}`;
}
