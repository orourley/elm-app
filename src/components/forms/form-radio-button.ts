import type { Params } from '../../component';
import { FormBaseInput } from './form-base-input';

export class FormRadioButton extends FormBaseInput<boolean> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the elements.
		this._inputElem = this.query('> input', HTMLInputElement);

		// Get the name, id, and value.
		this._inputElem.name = params.attributes.get('name') ?? '';
		this._inputElem.id = params.attributes.get('id') ?? '';
		this._inputElem.value = params.attributes.get('value') ?? '';

		// Get the label.
		const label = params.attributes.get('label');
		if (label !== undefined) {
			this.setHtml(label, this.query('> .label', Element), params.innerHtmlContext);
		}
		else {
			this.removeNode(this.query('> .label', Element));
		}

		// Get the checked attribute.
		const checked = params.attributes.get('checked');
		if (checked === 'true' || checked === '') {
			this.root.classList.add('checked');
			this._inputElem.checked = true;
		}

		// Set the event callbacks.
		this._onChanged = params.eventHandlers.get('changed');
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._inputElem.name = name;
		}
		else {
			this._inputElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._inputElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._inputElem.id = id;
			this.makeIdsUnique(this._inputElem);
		}
		else {
			this._inputElem.removeAttribute('id');
		}
	}

	/** Gets if it is checked. */
	getValue(): boolean {
		return this._inputElem.checked;
	}

	/** Sets if it is checked. */
	setValue(value: boolean) {
		if (this._inputElem.checked !== value) {
			this._inputElem.checked = value;
			this.onInput();
		}
	}

	/** Focuses the input. */
	focus(): void {
		this._inputElem.focus();
	}

	/** The event that calls the handler. */
	protected onInput(): void {
		this.root.classList.toggle('checked', this._inputElem.checked);
		this._onChanged?.(this);
	}

	/** The input element. */
	private _inputElem: HTMLInputElement;

	/** The changed callback. */
	private _onChanged?: ((input: FormRadioButton) => void) | undefined;

	protected static override html = /* html */`
		<label class="uniqueIds">
			<input type="radio" onchange="onInput" />
			<span class="label button"></span>
		</label>
	`;

	protected static override css = /* css */`
		.FormRadioButton {
			position: relative;
			display: inline-block;
		}
		.FormRadioButton > * {
			vertical-align: middle;
		}
		.FormRadioButton > input {
			position: absolute;
			left: 50%;
			top: 50%;
			width: 0;
			height: 0;
			opacity: 0;
		}
		.FormRadioButton > .label {
			display: inline-block;
		}
		`;
}
