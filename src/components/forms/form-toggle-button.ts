import type { Params } from '../../component';
import { FormBaseInput } from './form-base-input';

export class FormToggleButton extends FormBaseInput<boolean> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the elements.
		this._inputElem = this.root as HTMLInputElement;

		// Get the text from the inner html.
		this._inputElem.setAttribute('data-text', params.innerHtml);

		// Set all of the attributes directly to the input.
		for (const entry of params.attributes) {
			if (entry[0] === 'checked') {
				this._inputElem.checked = true;
			}
			else if (entry[0] === 'type') {
				continue;
			}
			else if (entry[0] === 'class' && entry[1] !== '') {
				this._inputElem.classList.add(...entry[1].split(/\s+/u));
			}
			else {
				this._inputElem.setAttribute(entry[0], entry[1]);
			}
		}

		// Set the event handlers.
		this._onChanged = params.eventHandlers.get('changed');
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._inputElem.name = name;
		}
		else {
			this._inputElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._inputElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._inputElem.id = id;
			this.makeIdsUnique(this._inputElem);
		}
		else {
			this._inputElem.removeAttribute('id');
		}
	}

	/** Gets the value. */
	getValue(): boolean {
		return this._inputElem.checked;
	}

	/** Sets the value. */
	setValue(value: boolean) {
		this._inputElem.checked = value;
	}

	/** Focuses the input. */
	focus(): void {
		this._inputElem.focus();
	}

	/** When the checkbox changes its input. */
	protected onInput(): void {
		this._onChanged?.(this);
	}

	/** The input element. */
	private _inputElem: HTMLInputElement;

	/** The changed callback. */
	private _onChanged?: ((input: FormToggleButton) => void) | undefined;

	protected static override html = /* html */`
		<input class="button" type="checkbox" onInput="onInput" />`;

	protected static svgHtml = /* html */`
		<svg xmlns='http://www.w3.org/2000/svg' width="100%" height="100%" viewBox="0 0 24 24"><path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" fill="none" d="M2 12l7 7L22 6"></path></svg>
		`.trim().replace(/"/gu, '\'');

	protected static override css = /* css */`
		.FormToggleButton {
			display: inline-flex;
			appearance: none;
			height: calc(1em + 2 * var(--spacing));
			line-height: 1em;
			place-content: center;
			padding: var(--spacing);
		}
		.FormToggleButton::before {
			content: url("data:image/svg+xml;utf8,${FormToggleButton.svgHtml}");
			display: inline;
			margin-right: var(--spacing);
			width: 1em;
			height: 1em;
			transform: scale(0);
			transition: .125s transform ease-in-out;
		}
		.FormToggleButton:checked::before {
			transform: scale(1);
		}
		.FormToggleButton::after {
			content: attr(data-text);
			display: inline;
			height: 1em;
		}
		`;
}
