import { MathHelper } from '@orourley/pine-lib';
import type { Params } from '../../component';
import { FormBaseInput } from './form-base-input';

export class FormNumberInput extends FormBaseInput<number | string> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the element.
		this._inputElem = this.root as HTMLInputElement;

		// Get the decimals.
		const decimals = params.attributes.get('decimals');
		if (decimals !== undefined && decimals !== '') {
			const decimalsAsNumber = parseInt(decimals);
			if (isNaN(decimalsAsNumber) || decimalsAsNumber < 0) {
				throw new Error('Decimals attribute must be a non-negative integer or undefined.');
			}
			this._decimals = decimalsAsNumber;
		}

		// Set the name, id, and value.
		const name = params.attributes.get('name');
		if (name !== undefined) {
			this.setName(name);
		}
		const id = params.attributes.get('id');
		if (id !== undefined) {
			this.setId(id);
		}
		const value = params.attributes.get('value');
		if (value !== undefined) {
			this.setValue(value);
		}

		// Set the event handlers.
		this._onChanged = params.eventHandlers.get('changed');
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._inputElem.name = name;
		}
		else {
			this._inputElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._inputElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._inputElem.id = id;
			this.makeIdsUnique(this._inputElem);
		}
		else {
			this._inputElem.removeAttribute('id');
		}
	}

	/** Gets the value. */
	getValue(): number | string {
		const value = this._inputElem.value;
		const valueAsNumber = parseFloat(value);
		if (isNaN(valueAsNumber)) {
			return value;
		}
		return valueAsNumber;
	}

	/** Sets the value. */
	setValue(value: number | string) {
		if (typeof value === 'number') {
			this._inputElem.value = value.toString();
		}
		else {
			this._inputElem.value = value;
		}
		this._updateValue();
	}

	/** Focuses the input. */
	focus(): void {
		this._inputElem.focus();
	}

	/** Gets the decimals. */
	getDecimals(): number | undefined {
		return this._decimals;
	}

	/** Sets the decimals. */
	setDecimals(decimals: number | undefined) {
		if (decimals !== undefined && (isNaN(decimals) || decimals < 0)) {
			throw new Error('Decimals must be a non-negative integer or undefined.');
		}
		this._decimals = decimals;
		this._updateValue();
	}

	/** When the input changes. */
	protected onInput(): void {
		this._onChanged?.(this);
	}

	/** When the input blurs. */
	protected onBlur(): void {
		this._updateValue();
	}

	/** Updates the value in case it is an equation. */
	private _updateValue(): void {

		// If it's an equation, set the result from the value.
		const value = this._inputElem.value;
		if (isNaN(Number(value)) && value.match(/^[0-9.*/() +-]+$/u) !== null) {
			try {
				// eslint-disable-next-line no-eval
				const result = eval(value);
				if (typeof result === 'number') {
					const roundedResult = MathHelper.round(result, 10);
					if (this._decimals !== undefined) {
						this._inputElem.value = roundedResult.toFixed(this._decimals);
					}
					else {
						this._inputElem.value = roundedResult.toString();
					}
					this._onChanged?.(this);
				}
			}
			catch { /* empty */ }
		}
	}

	/** The input element. */
	private _inputElem: HTMLInputElement;

	/** The number of decimals. */
	private _decimals: number | undefined;

	/** The changed callback. */
	private _onChanged?: ((input: FormNumberInput) => void) | undefined;

	protected static override html = /* html */`
		<input class="input" onInput="onInput" onBlur="onBlur" />`;

	protected static override css = /* css */`
		`;
}
