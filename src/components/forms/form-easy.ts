import { Component, type Params } from '../../component';
import { FormHelper, type FormValues } from '../../form-helper';
import { UserContent } from '../user-content';

export class FormEasy extends Component {

	/** The canceled callback. */
	onCanceled: ((form: FormEasy) => void) | undefined;

	/** The submit callback. Returns a string message. */
	onSubmitted: ((form: FormEasy, values: FormValues) => Promise<string | void>) | undefined;

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Save the entries user content.
		this._entries = this.queryComponent('.entries', UserContent);

		// Save the inner HTML context.
		this._innerHtmlContext = params.innerHtmlContext;

		// Get the event callbacks.
		this.onCanceled = params.eventHandlers.get('canceled');
		this.onSubmitted = params.eventHandlers.get('submitted');
		if (!this.onCanceled) {
			this.removeNode(this.query('.cancel', Element));
		}

		// Get the submit text.
		const submitText = params.attributes.get('submittext');
		if (submitText !== undefined) {
			this.setHtml(submitText, this.query('.submit', Element), params.innerHtmlContext);
		}

		// Add the html to the entries elem.
		if (params.innerHtml !== '') {
			this._entries.setHtml(params.innerHtml, undefined, params.innerHtmlContext);
		}
	}

	/** Gets if the form is enabled. */
	isEnabled(): boolean {
		return !this.root.classList.contains('disabled');
	}

	/** Sets the form to enabled or disabled. */
	setEnabled(enabled: boolean): void {
		const root = this.root;
		if (enabled) {
			root.classList.remove('disabled');
			root.style.opacity = '';
			root.style.pointerEvents = '';
			root.style.userSelect = '';
		}
		else {
			root.classList.add('disabled');
			root.style.opacity = '50%';
			root.style.pointerEvents = 'none';
			root.style.userSelect = 'none';
		}
		for (const input of this._entries.root.querySelectorAll('input')) {
			input.disabled = !enabled;
		}
		for (const textarea of this._entries.root.querySelectorAll('textarea')) {
			textarea.disabled = !enabled;
		}
		for (const button of this._entries.root.querySelectorAll('button')) {
			button.disabled = !enabled;
		}
		for (const select of this._entries.root.querySelectorAll('select')) {
			select.disabled = !enabled;
		}
		if (this.queryHas('.cancel')) {
			this.query('.cancel', HTMLButtonElement).disabled = !enabled;
		}
		this.query('.submit', HTMLButtonElement).disabled = !enabled;
	}

	/** Gets the entries. */
	getEntries(): UserContent {
		return this._entries;
	}

	/** Clears every value in the form. */
	clear(): void {
		for (const input of this._entries.root.querySelectorAll('input')) {
			if (input.type === 'checkbox' || input.type === 'radio') {
				input.checked = false;
			}
			else {
				input.value = '';
			}
		}
		for (const textarea of this._entries.root.querySelectorAll('textarea')) {
			textarea.value = '';
		}
		for (const button of this._entries.root.querySelectorAll('button')) {
			button.value = '';
		}
		for (const select of this._entries.root.querySelectorAll('select')) {
			select.value = '';
		}
	}

	/** Gets the message below the form inputs. */
	getMessage(): string {
		return this.query('.message', Element).innerHTML;
	}

	/** Sets the message below the form inputs. */
	setMessage(message: string, context?: Component): void {
		this.setHtml(message, this.query('.message', Element), context);
	}

	/** Sets the submit text. */
	setSubmitText(submitText: string, context: Component = this): void {
		this.setHtml(submitText, this.query('.submit', Element), context);
	}

	/** Sets the values of a form. */
	setValues(values: FormValues): void {
		this.clear();
		for (const [key, value] of Object.entries(values)) {
			try {
				const elem = this._entries.root.querySelector(`[name="${key}"]`);
				if (elem instanceof HTMLInputElement) {
					if (elem.type === 'checkbox') {
						if (typeof value !== 'boolean') {
							throw new Error(`Value for a checkbox must be a boolean. It was a ${typeof value}.`);
						}
						elem.checked = value;
					}
					else if (elem.type === 'radio') {
						if (typeof value !== 'string') {
							throw new Error(`Value for a radio button must be a string. It was a ${typeof value}.`);
						}
						const allRadioElems = this._entries.root.querySelectorAll<HTMLInputElement>(`input[name="${key}"]`);
						for (const radioElem of allRadioElems) {
							if (value === radioElem.value) {
								radioElem.checked = true;
							}
						}
					}
					else {
						if (typeof value !== 'string') {
							throw new Error(`Value for a text input must be a string. It was a ${typeof value}.`);
						}
						elem.value = value;
					}
				}
				else if (elem instanceof HTMLTextAreaElement) {
					if (typeof value !== 'string') {
						throw new Error(`Value for a text area must be a string. It was a ${typeof value}.`);
					}
					elem.value = value;
				}
			}
			catch (error) {
				if (error instanceof Error) {
					throw new Error(`Could not setValue for ${key}: ${error.message}`);
				}
			}
		}
	}

	/** Manually trigger the submit button. */
	async triggerSubmit(): Promise<void> {

		// Then trigger the function called when it is submitted.
		await this.submitPressed();

		// Scroll down to the submit button to see any message that appeared.
		this.query('.submit', HTMLElement).scrollIntoView();
	}

	/** When the cancel button is pressed. */
	protected cancelPressed(): void {

		// Clear the message and disable the form.
		this.setHtml('', this.query('.message', Element));
		this.setEnabled(false);

		// Call the canceled callback.
		if (this.onCanceled) {
			this.onCanceled(this);
		}
	}

	/** When the submit button is pressed. */
	protected async submitPressed(): Promise<void> {

		// Clear the message and disable the form.
		this.setHtml('', this.query('.message', Element));
		this.setEnabled(false);

		// Get the values.
		const values = FormHelper.getValues(this._entries.root);

		// Do the callback, returning a message.
		if (this.onSubmitted) {
			const message = await this.onSubmitted(this, values);

			// Set the message and enable the form.
			if (typeof message === 'string') {
				this.setHtml(message, this.query('.message', Element), this._innerHtmlContext);
			}
		}

		this.setEnabled(true);
	}

	/** The entries user content. */
	private _entries: UserContent;

	/** The inner HTML context saved for when a message is displayed. */
	private _innerHtmlContext: Component | undefined;

	protected static override html = /* html */`
		<div class="uniqueIds">
			<UserContent class="entries"></UserContent>
			<div class="message"></div>
			<div class="buttons">
				<button class="cancel button" onclick="cancelPressed">Cancel</button>
				<button class="submit button" onclick="submitPressed">Submit</button>
			</div>
		</div>`;

	protected static override css = /* css */`
		.FormEasy {
			display: flex;
			flex-direction: column;
			gap: 1rem;
		}
		.FormEasy > .message {
			grid-column: 1 / 3;
			opacity: 1;
			transition: opacity .125s;
		}
		.FormEasy > .message:empty {
			position: absolute;
			opacity: 0;
		}
		.FormEasy > .buttons {
			display: grid;
			grid-template-columns: 1fr 1fr;
			gap: .5rem;
		}
		.FormEasy > .buttons > .cancel {
			justify-self: start;
		}
		.FormEasy > .buttons > .submit {
			justify-self: end;
		}
		.FormEasy > .buttons > .submit:only-child {
			grid-column: 1 / 3;
			justify-self: start;
		}`;

	protected static override childComponentTypes: (typeof Component)[] = [
		UserContent
	];
}
