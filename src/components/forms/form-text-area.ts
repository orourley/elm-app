import { Sanitizer } from '@orourley/pine-lib';
import type { Params } from '../../component';
import { FormBaseInput } from './form-base-input';

export class FormTextArea extends FormBaseInput<string> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the elements.
		this._textAreaElem = this.query('> textarea', HTMLTextAreaElement);

		// Set all of the attributes directly to the textarea.
		for (const entry of params.attributes) {
			if (entry[0] === 'value') {
				continue;
			}
			else {
				this._textAreaElem.setAttribute(entry[0], entry[1]);
			}
		}

		// Set the event handlers.
		this._onChanged = params.eventHandlers.get('changed');
		this._onKeyDown = params.eventHandlers.get('keydown');
		if (this._onKeyDown) {
			this._textAreaElem.onkeydown = (event) => {
				this._onKeyDown!(this, event);
			};
		}

		// Set the value to any inner HTML.
		this._textAreaElem.value = Sanitizer.desanitizeFromHtml(params.innerHtml);

		// Call some update functions to clean things up.
		this.updateFromValue();
	}

	/** Gets the name. */
	getName(): string {
		return this._textAreaElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._textAreaElem.name = name;
		}
		else {
			this._textAreaElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._textAreaElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._textAreaElem.id = id;
			this.makeIdsUnique(this._textAreaElem);
		}
		else {
			this._textAreaElem.removeAttribute('id');
		}
	}

	/** Gets the value. */
	getValue(): string {
		return this._textAreaElem.value;
	}

	/** Sets the value. */
	setValue(value: string) {
		if (this._textAreaElem.value !== value) {
			this._textAreaElem.value = value;
			this.updateFromValue();
		}
	}

	/** Focuses the input. */
	focus(): void {
		this._textAreaElem.focus();
	}

	/** Gets the selection. */
	getSelection(): { start: number; end: number; } {
		return { start: this._textAreaElem.selectionStart, end: this._textAreaElem.selectionEnd };
	}

	/** Sets the selection. */
	setSelection(selection: { start: number; end: number; }): void {
		this._textAreaElem.selectionStart = selection.start;
		this._textAreaElem.selectionEnd = selection.end;
	}

	/** The event that calls the handler. */
	protected onChange(): void {
		this.updateFromValue();
		// Call the callback.
		this._onChanged?.(this);
	}

	/** Make sure the replicated-value is updated so that the other div's height is set. */
	private updateFromValue(): void {
		(this._textAreaElem.parentNode as HTMLElement).dataset['replicatedValue'] = this._textAreaElem.value;
	}

	/** The text area element. */
	private _textAreaElem: HTMLTextAreaElement;

	/** The changed callback. */
	private _onChanged: ((input: FormTextArea) => void) | undefined;

	/** The onkeydown callback. */
	private _onKeyDown: ((input: FormTextArea, event: KeyboardEvent) => void) | undefined;

	protected static override html = /* html */`
		<span class="textarea-grower" data-replicated-value="">
			<textarea class="input" rows=1 oninput="onChange" onchange="onChange"></textarea>
		</span>`;

	protected static override css = /* css */`
		.FormTextArea {
			display: inline-grid;
			width: 100%;
		}
		.FormTextArea::after {
			grid-area: 1 / 1 / 2 / 2;
			content: attr(data-replicated-value) " ";
			white-space: pre-wrap;
			visibility: hidden;
			padding: calc(var(--spacing) / 2) var(--spacing);
			line-height: calc(1em + var(--spacing));
			word-break: break-word;
		}
		.FormTextArea > textarea {
			grid-area: 1 / 1 / 2 / 2;
			margin: 0;
			width: 100%;
			height: auto;
			resize: none;
			overflow: hidden;
			padding: calc(var(--spacing) / 2) var(--spacing);
			line-height: calc(1em + var(--spacing));
			word-break: break-word;
		}`;
}
