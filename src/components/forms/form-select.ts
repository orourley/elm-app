import type { Component, Params } from '../../component';
import { scrollIntoView } from '../../scroll-into-view';
import { DropDown } from '../drop-down';
import { UserContent } from '../user-content';
import { FormBaseInput } from './form-base-input';

// A single option.
type Option = { value: string | undefined; label: string; depth: number; elem: HTMLOptionElement | undefined; };

/** A generic selection with an options drop down.
 * Use the option.selected and option.highlighted CSS classes. */
export class FormSelect extends FormBaseInput<string> {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Get the value and display input elements.
		this._inputElem = this.query('> input.value', HTMLInputElement);
		this._displayInputElem = this.query('> input.display', HTMLInputElement);

		// Set the name, id, and value.
		const name = params.attributes.get('name');
		if (name !== undefined) {
			this.setName(name);
		}
		const id = params.attributes.get('id');
		if (id !== undefined) {
			this.setId(id);
		}
		const value = params.attributes.get('value');
		if (value !== undefined) {
			this.setValue(value);
		}

		// Set whether or not to hide selections that don't match the current input.
		this._filter = params.attributes.has('filter');

		// Set the options html.
		this.setOptionsHtml(params.innerHtml, params.innerHtmlContext);

		// Set the option selected callback.
		this._onChanged = params.eventHandlers.get('changed');
	}

	/** Destroys the FormSelect. */
	protected override destroy(): void {
		if (this._dropDown) {
			this.app.closeOverlay(this._dropDown);
		}
	}

	/** Gets the name. */
	getName(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	setName(name: string) {
		if (name !== '') {
			this._inputElem.name = name;
		}
		else {
			this._inputElem.removeAttribute('name');
		}
	}

	/** Gets the id. */
	getId(): string {
		return this._displayInputElem.id;
	}

	/** Sets the id. */
	setId(id: string) {
		if (id !== '') {
			this._displayInputElem.id = id;
			this.makeIdsUnique(this._displayInputElem);
		}
		else {
			this._displayInputElem.removeAttribute('id');
		}
	}

	/** Gets the value. */
	getValue(): string {
		return this._inputElem.value;
	}

	/** Sets the value. */
	setValue(value: string) {

		// Set the value.
		this._inputElem.value = value;

		// If the selection no longer matches the value, select the first option with the given value (or undefined if there are none).
		if (this._selectedOption?.value !== value) {
			const newSelectedOption = this._options.find((option) => option.value === value);
			this.selectOption(newSelectedOption);
		}
	}

	/** Focuses the input. */
	focus(): void {
		this._displayInputElem.focus();
	}

	/** Set the html of the drop down. */
	setOptionsHtml(optionsHtml: string, optionsHtmlContext: Component | undefined): void {

		// Update the variables.
		this._optionsHtml = optionsHtml;
		this._optionsHtmlContext = optionsHtmlContext;

		// Clear the filtered options.
		for (const filteredOutOption of this._filteredOutOptions) {
			filteredOutOption.elem?.classList.remove('hidden');
		}
		this._filteredOutOptions.clear();

		// Create a staging area with the html content to extract the options info.
		const stagingElem = this.query('.staging', Element);
		this.setHtml(this._optionsHtml, stagingElem, this._optionsHtmlContext);

		// Go through each option and collect their info.
		// Using querySelectorAll to grab options inside any component.
		this._options = [];
		for (const optionElem of stagingElem.querySelectorAll('option')) {

			this._options.push({
				value: optionElem.getAttribute('value') !== null ? optionElem.value : undefined,
				label: optionElem.innerText,
				depth: parseInt(optionElem.getAttribute('depth') ?? '0'),
				elem: undefined
			});
		}

		// Remove the staging elem html.
		this.clearNode(stagingElem);

		// If we had a value, try to update it to the first elem that has that same value and label.
		if (this._inputElem.value !== '') {
			const selectedOption = this._options.find((option) =>
				option.value === this._inputElem.value && (!this._selectedOption || option.label === this._selectedOption.label));

			// Update the selection.
			this.selectOption(selectedOption);
		}

		// If the drop down was on, close and reopen it to update its content.
		if (this._dropDown) {
			this._dropDown.close();
			this.openDropDown();
		}
	}

	/** Called when an option is clicked. */
	protected onClickOption(option: Option, event: Event): void {
		event.preventDefault();
		this.selectOption(option);
		this._dropDown?.close();
	}

	/** Opens the drop down. */
	protected openDropDown(): void {

		// First close any existing drop down.
		if (this._dropDown) {
			this._dropDown.close();
		}

		// Open the drop down.
		this._dropDown = this.app.openOverlay<DropDown>(`<DropDown class="FormSelectDropDown input">${this._optionsHtml}</DropDown>`, this._optionsHtmlContext);
		this._dropDown.setRelativeTo(this._displayInputElem);
		this._dropDown.setOnClosedHandler(this.onDropDownClosed.bind(this));

		// Populate the options elems.
		const optionElems = this._dropDown.root.querySelectorAll('option');
		for (const optionElem of optionElems) {
			const option = this._options.find((o) => (o.value === undefined || o.value === optionElem.value) && o.label === optionElem.textContent);
			if (!option) {
				continue;
			}
			option.elem = optionElem;
			if (this._filteredOutOptions.has(option)) {
				option.elem.classList.add('hidden');
			}

			// Add the event listeners, if needed.
			if (option.value !== undefined) {
				optionElem.addEventListener('mouseover', this.highlightOption.bind(this, option));
				optionElem.addEventListener('mouseleave', this.highlightOption.bind(this, undefined));
				optionElem.addEventListener('click', this.onClickOption.bind(this, option));
			}
		}
	}

	/** When the drop down is closed. */
	protected onDropDownClosed(): void {
		this._dropDown = undefined;

		// Clear out the options elems.
		for (const option of this._options) {
			option.elem = undefined;
		}
	}

	/** Highlights an option. */
	protected highlightOption(option: Option | undefined): void {
		if (this._highlightedOption !== undefined) {
			this._highlightedOption.elem?.classList.remove('highlighted');
		}
		this._highlightedOption = option;
		if (this._highlightedOption !== undefined) {
			this._highlightedOption.elem?.classList.add('highlighted');
		}
	}

	/** Handles key presses for the display. */
	protected onDisplayKeyDown(event: KeyboardEvent): void {
		if (event.key === 'Tab' && this._dropDown) {
			this._dropDown.close();
		}
		else if (!this._dropDown) {
			this.openDropDown();
		}
		if (event.key === 'Enter' || event.key === 'Space') {
			if (this._selectedOption) {
				this._selectedOption.elem?.click();
			}
		}
		else if (event.key === 'ArrowUp') {
			event.preventDefault();

			// Select the previous option.
			const indexOfSelectedOption = this._selectedOption ? this._options.indexOf(this._selectedOption) : this._options.length;
			for (let i = 1; i < this._options.length; i++) {
				const option = this._options[(indexOfSelectedOption - i + this._options.length) % this._options.length]!;
				if (!this._filteredOutOptions.has(option)) {
					this.selectOption(option);
					break;
				}
			}

			// Make sure the new option is visible.
			if (this._selectedOption && this._dropDown && this._selectedOption.elem) {
				scrollIntoView(this._selectedOption.elem, this._dropDown.root);
			}
		}
		else if (event.key === 'ArrowDown') {
			event.preventDefault();

			// Select the previous option.
			const indexOfSelectedOption = this._selectedOption ? this._options.indexOf(this._selectedOption) : -1;
			for (let i = 1; i < this._options.length; i++) {
				const option = this._options[(indexOfSelectedOption + i + this._options.length) % this._options.length]!;
				if (!this._filteredOutOptions.has(option)) {
					this.selectOption(option);
					break;
				}
			}

			// Make sure the new option is visible.
			if (this._selectedOption && this._dropDown && this._selectedOption.elem) {
				scrollIntoView(this._selectedOption.elem, this._dropDown.root);
			}
		}
	}

	/** Called when the input changes, to handle filtering. */
	protected inputChanged(): void {

		// Get the current label text.
		const label = this._displayInputElem.value.toLocaleLowerCase();

		// Go through each option,
		for (let i = 0, l = this._options.length; i < l; i++) {

			// Get the option, value, and label.
			const option = this._options[i]!;
			const optionLabel = option.label.toLocaleLowerCase();

			// If we're filtering, see if this option needs to be hidden.
			if (this._filter) {

				// Check if this option's label or any of its ancestors' labels include the label text.
				let show = false;
				if (optionLabel.includes(label)) {
					show = true;
				}
				else {
					let depth = option.depth;
					for (let j = i - 1; j >= 0; j--) {
						const otherOption = this._options[j]!;
						if (otherOption.depth < depth) {
							if (otherOption.label.toLocaleLowerCase().includes(label)) {
								show = true;
								break;
							}
							depth = otherOption.depth;
							if (depth === 0) {
								break;
							}
						}
					}
				}

				// If we can show this option,
				if (show) {

					// Remove any prior filteredOut class.
					if (this._filteredOutOptions.has(option)) {
						option.elem?.classList.remove('hidden');
						this._filteredOutOptions.delete(option);
					}

					// Unfilter options higher up in a hierarchy.
					let depth = option.depth;
					for (let j = i - 1; j >= 0; j--) {
						const otherOption = this._options[j]!;
						if (otherOption.depth < depth) {
							if (this._filteredOutOptions.has(otherOption)) {
								otherOption.elem?.classList.remove('hidden');
								this._filteredOutOptions.delete(otherOption);
							}
							depth = otherOption.depth;
							if (depth === 0) {
								break;
							}
						}
					}
				}
				else {
					option.elem?.classList.add('hidden');
					this._filteredOutOptions.add(option);
				}
			}

			// If the option exactly matches the text, select it.
			if (optionLabel.toLocaleLowerCase() === label.toLocaleLowerCase()) {
				this.selectOption(option);
			}
		}
	}

	/** Select an option. */
	private selectOption(option: Option | undefined): void {

		// Deselect the previous option.
		if (this._selectedOption) {
			this._selectedOption.elem?.classList.remove('selected');
		}

		// Select the new option.
		this._selectedOption = option;
		if (!this._selectedOption) {
			return;
		}

		// Mark it as selected.
		this._selectedOption.elem?.classList.add('selected');

		// If the option has a value, update the input and display elements.
		if (this._selectedOption.value !== undefined) {
			this._inputElem.value = this._selectedOption.value;
			this._displayInputElem.value = this._selectedOption.label;

			// Call the callback.
			this._onChanged?.(this);
		}
	}

	/** The html to show when opening the drop down. */
	private _optionsHtml: string = '';

	/** The html context to use when opening the drop down. */
	private _optionsHtmlContext: Component | undefined;

	/** The drop down, when opened. */
	private _dropDown: DropDown | undefined;

	/** All of the options. */
	private _options: Option[] = [];

	/** Whether or not to hide selections that don't match the current input. */
	private _filter: boolean;

	/** The input element with the name and value. Its value can be anything, even if there are no matching options. */
	private _inputElem: HTMLInputElement;

	/** The input element with the display text. */
	private _displayInputElem: HTMLInputElement;

	/** The currently highlighted option. */
	private _highlightedOption: Option | undefined;

	/** The currently selected option. Will only be undefined if there are no options with the given value. */
	private _selectedOption: Option | undefined;

	/** The currently filtered out options. */
	private _filteredOutOptions = new Set<Option>();

	/** The changed callback. */
	private _onChanged: ((input: FormSelect) => void) | undefined;

	protected static override html = /* html */`
		<span>
			<input class="value" type="hidden"></input>
			<input class="display input" onkeydown="onDisplayKeyDown" onfocus="openDropDown" oninput="inputChanged"></input>
			<div class="hidden staging"></div>
		</span>
		`;

	protected static override css = /* css */`
		.FormSelect {
			display: inline-block;
			position: relative;
		}
		.FormSelect > .display {
			display: inline-block;
			width: 100%;
		}
		.overlays > .FormSelectDropDown {
			position: absolute;
			overflow-y: auto;
			max-height: 15rem;
		}
		.overlays > .FormSelectDropDown.hidden {
			display: none;
		}
		.overlays > .FormSelectDropDown option {
			cursor: pointer;
		}
		.overlays > .FormSelectDropDown .filteredOut {
			display: none;
		}
		`;

	protected static override childComponentTypes: (typeof Component)[] = [
		DropDown,
		UserContent
	];
}
