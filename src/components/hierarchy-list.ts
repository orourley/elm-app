import { Component, type Params } from '../component';
import { UserContent } from './user-content';

/** A hierarchical list for dragging items. */
export class HierarchyList extends Component {

	/** The constructor. */
	constructor(params: Params) {
		super(params);

		// Bind this functions.
		this._onGrab = this._onGrab.bind(this);
		this._onDrag = this._onDrag.bind(this);
		this._onRelease = this._onRelease.bind(this);

		// Save the user content as a variable.
		this._userContent = this.queryComponent('> .UserContent', UserContent);

		// Get whether or not the grab button is visible.
		this._grabButtonVisible = params.attributes.get('nograb') === undefined;

		// Save the filter elem callback.
		this._filterElem = params.eventHandlers.get('filterelem') ?? ((): boolean => true);

		// Save the on item moved callback.
		this._onItemMoved = params.eventHandlers.get('itemmoved') ?? ((): void => {});

		// Set the user content to the given html.
		this._userContent.setHtml(params.innerHtml, undefined, params.innerHtmlContext);
	}

	protected override destroy(): void {
		// Remove the scroll interval.
		window.clearInterval(this._updateScrollInterval);
		// Clean up the event listeners.
		window.removeEventListener('mousemove', this._onDrag);
		window.removeEventListener('touchmove', this._onDrag);
		window.removeEventListener('mouseup', this._onRelease);
		window.removeEventListener('touchend', this._onRelease);
	}

	/** Gets the content. */
	getContent(): UserContent {
		return this._userContent;
	}

	/** Gets whether or not the grab buttons are visible. */
	getGrabButtonVisible(): boolean {
		return this._grabButtonVisible;
	}

	/** Sets whether or not the grab buttons are visible. */
	setGrabButtonVisible(grabButtonVisible: boolean): void {
		this._grabButtonVisible = grabButtonVisible;
		const grabElems = this._userContent.root.querySelectorAll('.grab');
		for (const grabElem of grabElems) {
			if (grabElem instanceof HTMLElement || grabElem instanceof SVGElement) {
				grabElem.classList.toggle('hidden', !this._grabButtonVisible);
			}
		}
	}

	/** When the content changes, update the grab event listeners. */
	protected onContentChange(): void {

		// Set up all grab elements.
		const grabElems = this._userContent.root.querySelectorAll('.grab');
		for (const grabElem of grabElems) {
			if ((grabElem instanceof HTMLElement || grabElem instanceof SVGElement) && grabElem.dataset['processed'] !== 'true') {
				grabElem.addEventListener('mousedown', this._onGrab);
				grabElem.addEventListener('touchstart', this._onGrab);
				grabElem.setAttribute('tabIndex', '-1');
				grabElem.classList.toggle('hidden', !this._grabButtonVisible);
				grabElem.dataset['processed'] = 'true';
			}
		}
	}

	/** When the dragger has started dragging. */
	private _onGrab(event: Event): void {
		// If the event isn't cancelable, something else is happening so don't initiate a drag.
		if (!event.cancelable) {
			return;
		}
		// First lose focus on anything so that dragging doesn't jolt the screen around.
		if (document.activeElement instanceof HTMLElement) {
			document.activeElement.blur();
		}
		// Get the item associated with the grab element.
		let draggedItem = event.target as Element;
		while (!(draggedItem instanceof HTMLLIElement)) {
			const parentElem = draggedItem.parentElement;
			if (!parentElem) {
				return;
			}
			draggedItem = parentElem;
		}
		this._draggedItem = draggedItem;
		// Create the insert line element.
		const rulerLine = document.createElement('div');
		rulerLine.classList.add('ruler');
		this._insertLineElem = document.createElement('div');
		this._insertLineElem.classList.add('insertLine');
		this._insertLineElem.appendChild(rulerLine);
		this._draggedItem.insertBefore(this._insertLineElem, this._draggedItem.firstChild);
		this._draggedItem.classList.add('dragging');

		// Find the most descendent scrollable element for use in scrolling to keep the cursor in view.
		this._scrollableElem = this.root;
		while (true) {
			if (this._scrollableElem === document.body) {
				break;
			}
			const overflowY = getComputedStyle(this._scrollableElem).overflowY;
			if (overflowY === 'scroll' || overflowY === 'auto') {
				break;
			}
			this._scrollableElem = this._scrollableElem.parentElement!;
		}

		// Setup the potential lines. They will be used for figuring out where to insert the insertLineElem.
		this._potentialLinePos = [];
		let elems = this._userContent.queryAll(':is(ol, ul):not(:has(li)), li', HTMLElement);
		// Filter out liElems that are inside the draggedElem.
		elems = elems.filter((elem) => !this._draggedItem!.contains(elem) || this._draggedItem === elem);
		const rootBounds = this.root.getBoundingClientRect();
		for (const elem of elems) {
			if (this._draggedItem.contains(elem) && this._draggedItem !== elem) {
				continue;
			}
			const elemBounds = elem.getBoundingClientRect();
			if (elem instanceof HTMLLIElement) {
				if (this._filterElem(elem, true, this._draggedItem)) {
					this._potentialLinePos.push({
						pos: [Math.round(elemBounds.left - rootBounds.left), Math.round(elemBounds.top - rootBounds.top)],
						elem,
						where: 'before'
					});
				}
				if (!elem.nextElementSibling && this._filterElem(elem, false, this._draggedItem)) {
					this._potentialLinePos.push({
						pos: [Math.round(elemBounds.left - rootBounds.left), Math.round(elemBounds.bottom - rootBounds.top)],
						elem,
						where: 'after'
					});
				}
			}
			else if (this._filterElem(elem, false, this._draggedItem)) {
				this._potentialLinePos.push({
					pos: [Math.round(elemBounds.left - rootBounds.left), Math.round(elemBounds.bottom - rootBounds.top)],
					elem,
					where: 'after'
				});
			}
		}

		// Start the scrolling for the cursor.
		this._cursorPos = this._getPosFromEvent(event);
		this._updateScrollInterval = window.setInterval(this._scrollToCursor.bind(this));

		// Enable the drag callbacks.
		window.addEventListener('mousemove', this._onDrag);
		window.addEventListener('touchmove', this._onDrag);
		window.addEventListener('mouseup', this._onRelease);
		window.addEventListener('touchend', this._onRelease);

		// Update the insertLineElem.
		this._onDrag(event);

		// Make sure nothing else happens.
		event.preventDefault();
	}

	/** When the dragger is moved. */
	private _onDrag(event: Event): void {
		if (!this._insertLineElem || !this._draggedItem) {
			return;
		}

		// Get the position of the cursor.
		const rootBounds = this.root.getBoundingClientRect();
		this._cursorPos = this._getPosFromEvent(event);

		// Go through each potential line and find the closest.
		let closest;
		let closestDiff: [number, number] = [Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY];
		for (const potentialLine of this._potentialLinePos) {
			const diff: [number, number] = [
				Math.abs(this._cursorPos[0] - rootBounds.left - potentialLine.pos[0]),
				Math.abs(this._cursorPos[1] - rootBounds.top - potentialLine.pos[1])];
			if (diff[1] > closestDiff[1]) {
				continue;
			}
			if (diff[1] < closestDiff[1] || diff[0] < closestDiff[0]) {
				closestDiff = diff;
				closest = potentialLine;
			}
		}
		if (!closest) {
			return;
		}

		// Once we've found the closest, insert the inesertLineElem at that spot.
		if (closest.where === 'before') {
			closest.elem.insertBefore(this._insertLineElem, closest.elem.firstChild);
		}
		else {
			closest.elem.insertBefore(this._insertLineElem, null);
		}
	}

	/** Scroll the scrollable element toward the cursor. */
	private _scrollToCursor(): void {
		if (this._scrollableElem !== undefined) {
			// Get the scrollable elem bounds relative to the viewport.
			const scrollableBounds = this._scrollableElem.getBoundingClientRect();
			// Scroll down if needed.
			const bottomDiff = this._cursorPos[1] - (scrollableBounds.bottom - 16);
			if (bottomDiff > 0) {
				const scrollChange = Math.round(bottomDiff / 1.5);
				this._scrollableElem.scrollTop += scrollChange;
			}
			// Scroll up if needed.
			const topDiff = this._cursorPos[1] - (scrollableBounds.top + 16);
			if (topDiff < 0) {
				const scrollChange = Math.max(Math.round(topDiff / 1.5), -this._scrollableElem.scrollTop);
				this._scrollableElem.scrollTop += scrollChange;
			}
		}
	}

	/** When the dragger is released. */
	private _onRelease(event: Event): void {

		// Remove the scroll interval.
		window.clearInterval(this._updateScrollInterval);
		// Clean up the event listeners.
		window.removeEventListener('mousemove', this._onDrag);
		window.removeEventListener('touchmove', this._onDrag);
		window.removeEventListener('mouseup', this._onRelease);
		window.removeEventListener('touchend', this._onRelease);

		// Clear the dragged item class.
		if (!this._draggedItem) {
			return;
		}
		this._draggedItem.classList.remove('dragging');

		// If the insert line elem is inside the dragged elem, do nothing.
		if (!this._insertLineElem) {
			return;
		}
		if (this._draggedItem.contains(this._insertLineElem)) {
			this._insertLineElem.remove();
			return;
		}

		// Insert the item based on where the insert line elem was, and call the callback.
		const insertItem = this._insertLineElem.parentElement as HTMLLIElement | HTMLUListElement | HTMLOListElement;
		const insertItemBefore = insertItem.firstElementChild === this._insertLineElem;
		this._insertLineElem.remove();
		if (insertItem instanceof HTMLLIElement) {
			const insertItemParent = insertItem.parentElement?.parentElement instanceof HTMLLIElement ? insertItem.parentElement.parentElement : undefined;
			if (insertItemBefore) {
				insertItem.before(this._draggedItem);
				this._onItemMoved(this._draggedItem, insertItemParent, insertItem);
			}
			else {
				insertItem.after(this._draggedItem);
				this._onItemMoved(this._draggedItem, insertItemParent, undefined);
			}
		}
		else {
			insertItem.append(this._draggedItem);
			this._onItemMoved(this._draggedItem, insertItem.parentElement as HTMLLIElement, undefined);
		}
		if (event.cancelable) {
			event.preventDefault();
		}
	}

	/** Gets the position of the event relative to the viewport. */
	private _getPosFromEvent(event: Event): [number, number] {
		if (event instanceof MouseEvent) {
			return [event.clientX, event.clientY];
		}
		else if (event instanceof TouchEvent) {
			if (event.touches.length > 0) {
				const firstTouch = event.touches[0]!;
				return [firstTouch.clientX, firstTouch.clientY];
			}
		}
		return [NaN, NaN];
	}

	/** The user content. */
	private _userContent: UserContent;

	/** A flag whether or not the grab button is visible. */
	private _grabButtonVisible: boolean;

	/** The item currently being dragged. */
	private _draggedItem: HTMLLIElement | undefined;

	private _potentialLinePos: { pos: [number, number]; elem: HTMLElement; where: 'before' | 'after'; }[] = [];

	/** The insert line element. */
	private _insertLineElem: HTMLDivElement | undefined;

	/** The cursor's original value relative to the viewport. */
	private _cursorPos: [number, number] = [0, 0];

	/** The most descendent scrollable element. */
	private _scrollableElem: Element | undefined;

	/** The interval for adjusting the scroll of the scrollable element. */
	private _updateScrollInterval: number | undefined;

	/** A callback to filter out an elem for potential lines. */
	private _filterElem: (elems: HTMLElement, before: boolean, draggedItem: HTMLLIElement) => boolean;

	/** The callback when an item is moved to another spot. */
	private _onItemMoved: (item: HTMLLIElement, parentItem: HTMLLIElement | undefined, beforeItem: HTMLLIElement | undefined) => void;

	protected static override html = `
		<div>
			<UserContent onChange="onContentChange"></UserContent>
		</div>
		`;

	protected static override css = `
		.HierarchyList .grab.hidden {
			display: none;
		}
		.HierarchyList li.dragging {
			opacity: 50%;
		}
		.HierarchyList .insertLine {
			position: relative;
			width: 10rem;
			height: 0;
			outline: 0.25rem solid currentColor;
			background: rgba(255, 255, 0, 0.5);
			z-index: 10;
		}
		.HierarchyList .insertLine .ruler {
			position: absolute;
			height: 20rem;
			top: -10rem;
			border-left: 0.25rem dashed currentColor;
		}
		`;

	protected static override childComponentTypes = [
		UserContent
	];
}
