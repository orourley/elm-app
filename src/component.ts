import { RandomString } from './utils/random-string';

// Not importing App directly to avoid circular dependencies.
import type { App } from './app';

/** A type for when we add component types to window.UI. */
type WindowWithComponents = Window & typeof globalThis & { UI: Record<string, typeof Component> | undefined; };

/**
 * A base component from which other components can extend.
 * Each subclass can have an `html` and a `css` property to add content and style to the components.
 * Only the most derived subclass's `html` property will be used.
 * If the component is an App, its root will be the body tag.
 */
export class Component {

	/** The parent component. */
	readonly parent: Component | undefined;

	/** The app component. */
	readonly app: App;

	/** The root element. */
	protected readonly root: HTMLElement;

	/** Creates the component. */
	constructor(params: Params) {

		// Set the parent of the component.
		this.parent = params.parent;

		// Set the app of the component.
		if (!params.parent) {
			this.app = (this as Component) as App;
		}
		else {
			this.app = params.parent.app;
		}

		// Get the registery entry. If it doesn't exist, register it.
		if (!Component._registry.has(this.constructor.name.toLowerCase())) {
			(this.constructor as typeof Component).register();
		}
		this._registryEntry = Component._registry.get(this.constructor.name.toLowerCase())!;

		// Create the template and add the html content as the root node.
		// If it's an app, use the body as the root.
		const templateElem = document.createElement('template');
		templateElem.innerHTML = this._registryEntry.html;
		if (!params.parent) {
			document.body.innerHTML = '';
			document.body.append(...templateElem.content.childNodes);
			this.root = document.body;
		}
		else {
			if (templateElem.content.childNodes.length !== 1) {
				throw new Error(`In ${this.constructor.name}, there must exactly be one root in the HTML.`);
			}
			const root = templateElem.content.childNodes[0];
			if (!(root instanceof HTMLElement)) {
				throw new Error(`In ${this.constructor.name}, the root node must be an HTMLElement type.`);
			}
			this.root = root;
		}

		// Set the elements that are child components and the event handlers.
		// The root may be a component itself, so it needs to be adjusted.
		this.root = this.setComponentsAndEventHandlers(this.root, this);

		// Add the component name and its ancestors as classes to the root element.
		for (const ancestorEntry of this._registryEntry.ancestorEntries) {
			this.root.classList.add(ancestorEntry.ComponentType.name);
		}

		// Add any extra classes given in the params.
		for (const className of params.classes) {
			this.root.classList.add(className);
		}

		// Set the style elements for the component and its ancestors.
		// Super and child components are earlier than others.
		for (let i = this._registryEntry.ancestorEntries.length - 1; i >= 0; i--) {
			const ancestorEntry = this._registryEntry.ancestorEntries[i]!;

			// Create the ancestor's style element if it doesn't already exist, and increment the use count.
			if (ancestorEntry.css !== undefined) {
				if (ancestorEntry.styleElemUseCount === 0) {
					ancestorEntry.styleElem = document.createElement('style');
					ancestorEntry.styleElem.classList.add(`${ancestorEntry.ComponentType.name}Style`);
					ancestorEntry.styleElem.innerHTML = ancestorEntry.css;
					document.head.appendChild(ancestorEntry.styleElem);
				}
				ancestorEntry.styleElemUseCount += 1;
			}
		}
	}

	/** Destroys the component when it is no longer needed. Call to clean up the object. */
	protected destroy(): void {

		// Destroy all of the child components.
		for (const component of this._elemsToComponents.values()) {
			component.destroy();
		}

		// Remove the style elements of the component and its ancestors.
		for (let i = 0; i < this._registryEntry.ancestorEntries.length; i++) {
			// Decrement the use count of the ancestor's style element and remove it if the use count is zero.
			const ancestorEntry = this._registryEntry.ancestorEntries[i]!;
			if (ancestorEntry.styleElem !== undefined) {
				ancestorEntry.styleElemUseCount -= 1;
				if (ancestorEntry.styleElemUseCount === 0) {
					document.head.removeChild(ancestorEntry.styleElem);
					ancestorEntry.styleElem = undefined;
				}
			}
		}
	}

	/** Gets the element that contains the component's root. */
	getParentElement(): HTMLElement {
		return this.root.parentElement!;
	}

	/** Gets whether or not the root has the class. */
	hasClass(className: string): boolean {
		return this.root.classList.contains(className);
	}

	/** Sets whether or not the root has the given class. */
	setClass(className: string, has: boolean): void {
		this.root.classList.toggle(className, has);
	}

	/** Turns html into a list of nodes. It will not handle event handlers or inner components. Can be later be used with insertNode. */
	static htmlToNodes(html: string): Node[] {
		// Turn the HTML into a template.
		html = html.replace(/>[\t\n]+</gu, '><').trim();
		const templateElem = document.createElement('template');
		templateElem.innerHTML = html;
		return Array.from(templateElem.content.childNodes);
	}

	/** Gets an element by a query. The selector `:scope ` is prepended to the query. If scope is undefined, the root is used. */
	protected query<Type extends Element>(query: string, type: abstract new (...params: unknown[]) => Type, scope: Element = this.root): Type {
		if (!this.isNodeInThis(scope)) {
			throw new Error('The scope is not in this component.');
		}
		const element = scope.querySelector(`:scope ${query}`);
		if (element === null || !this.isNodeInThis(element)) {
			throw new ReferenceError(`The element with query "${query}" does not exist.`);
		}
		if (!(element instanceof type)) {
			throw new ReferenceError(`The element with query "${query}" is not of type ${type.name}.`);
		}
		return element;
	}

	/** Returns true if the query exists. The selector `:scope ` is prepended to the query. If scope is undefined, the root is used. */
	protected queryHas(query: string, root: Element = this.root): boolean {
		const element = root.querySelector(`:scope ${query}`);
		return element !== null && this.isNodeInThis(element);
	}

	/** Gets all elements by a query. The selector `:scope ` is prepended to the query. */
	protected queryAll<Type extends Element>(query: string, type: abstract new (...args: unknown[]) => Type, scope: Element = this.root): Type[] {
		if (!this.isNodeInThis(scope)) {
			throw new Error('The scope is not in this component.');
		}
		const elements = [...scope.querySelectorAll(`:scope ${query}`)];
		for (let i = 0; i < elements.length; i++) {
			const element = elements[i]!;
			if (!this.isNodeInThis(element)) {
				elements.splice(i, 1);
				i--;
			}
			else if (!(element instanceof type)) {
				throw new ReferenceError(`An element with queryAll "${query}" is not of type ${type.name}.`);
			}
		}
		return elements as Type[];
	}

	/** Gets a component by a query. The selector `:scope ` is prepended to the query. If scope is undefined, the root is used. */
	protected queryComponent<Type extends Component>(query: string | Element, type: abstract new (params: Params) => Type,
		scope: Element = this.root): Type {
		if (!this.isNodeInThis(scope)) {
			throw new Error('The scope is not in this component.');
		}
		let element: Element | null = null;
		if (query instanceof Element) {
			element = query;
		}
		else {
			const elements = [...scope.querySelectorAll(`:scope ${query}`)];
			while (!element && elements.length > 0) {
				const element0 = elements.shift();
				if (element0 && this._elemsToComponents.has(element0)) {
					element = element0;
				}
			}
		}
		if (element === null) {
			throw new ReferenceError(`The element with query "${typeof query === 'string' ? query : query.tagName}" does not exist.`);
		}
		const component = this._elemsToComponents.get(element);
		if (component === undefined) {
			throw new ReferenceError(`The component with query "${typeof query === 'string' ? query : query.tagName}" does not exist.`);
		}
		if (!(component instanceof type)) {
			throw new ReferenceError(`The component of type ${component.constructor.name} is not of type ${type.name}.`);
		}
		return component;
	}

	/** Returns true if the component with the query exists. The selector `:scope ` is prepended to the query. If scope is undefined, the root is used. */
	protected queryHasComponent(query: string | Element, type: abstract new (params: Params) => Component, scope: Element = this.root): boolean {
		if (!this.isNodeInThis(scope)) {
			throw new Error('The scope is not in this component.');
		}
		let element = null;
		if (query instanceof Element) {
			element = query;
		}
		else {
			const elements = [...scope.querySelectorAll(`:scope ${query}`)];
			while (!element && elements.length > 0) {
				const element0 = elements.shift();
				if (element0 && this._elemsToComponents.has(element0)) {
					element = element0;
				}
			}
		}
		if (element === null) {
			return false;
		}
		const component = this._elemsToComponents.get(element);
		if (component === undefined) {
			return false;
		}
		if (!(component instanceof type)) {
			return false;
		}
		return true;
	}

	/** Gets all components by a query. The selector `:scope ` is prepended to the query. If scope is undefined, the root is used. */
	protected queryAllComponents<Type extends Component>(query: string, type: abstract new (params: Params) => Type, scope: Element = this.root): Type[] {
		if (!this.isNodeInThis(scope)) {
			throw new Error('The scope is not in this component.');
		}
		const components: Type[] = [];
		const elements = scope.querySelectorAll(`:scope ${query}`);
		for (const element of elements) {
			const component = this._elemsToComponents.get(element);
			if (component === undefined) {
				continue;
			}
			if (!(component instanceof type)) {
				throw new ReferenceError(`The component of type ${component.constructor.name} is not of type ${type.name}.`);
			}
			components.push(component);
		}
		return components;
	}

	/** Sets the inner html for an element. Cleans up tabs and newlines in the HTML.
	 * Cleans up old ids, handlers, and components and adds new ids, handlers, and components. */
	protected setHtml(html: string, element: Element, context?: Component): void {
		if (!this.isNodeInThis(element)) {
			throw new Error('The element is not in this component.');
		}
		this.clearNode(element);
		this.insertHtml(html, element, undefined, context);
	}

	/** Inserts html at the end of the parent or before a child node. Returns the nodes of the inserted HTML. */
	protected insertHtml(html: string, parent: Node = this.root, before?: Node | Component, context?: Component): Node[] {
		if (!this.isNodeInThis(parent)) {
			throw new Error('The parent is not in this component.');
		}
		if (before instanceof Component) {
			if (!this._elemsToComponents.has(before.root)) {
				throw new Error('The before component is not in this component.');
			}
		}
		else if (before && !this.isNodeInThis(before)) {
			throw new Error('The before node is not in this component.');
		}
		// Get the nodes from the HTML.
		const nodes = Component.htmlToNodes(html);
		// Insert the nodes, replacing any with components, if needed.
		for (let i = 0; i < nodes.length; i++) {
			nodes[i] = this.insertNode(nodes[i]!, parent, before, context);
		}
		// Return the root nodes of the HTML.
		return nodes;
	}

	/** Inserts a node at the end of the parent or before a child node.
	 *  Returns the node, or its replacement if a component was created. */
	protected insertNode(node: Node, parent: Node = this.root, before?: Node | Component, context?: Component): Node {
		if (!this.isNodeInThis(parent)) {
			throw new Error('The parent is not in this component.');
		}
		if (before instanceof Component) {
			if (!this._elemsToComponents.has(before.root)) {
				throw new Error('The before component is not in this component.');
			}
			before = before.root;
		}
		else if (before && !this.isNodeInThis(before)) {
			throw new Error('The before node is not in this component.');
		}
		parent.insertBefore(node, before ?? null);
		if (node instanceof HTMLElement) {
			this.makeIdsUnique(node);
			return this.setComponentsAndEventHandlers(node, context);
		}
		return node;
	}

	/** Removes a node. */
	protected removeNode(node: Node): void {

		// Verify the node.
		if (!this.isNodeInThis(node)) {
			throw new Error('The node is not in this component.');
		}

		// Delete all of the components that are in the node. */
		const componentElementsToRemove = [];
		for (const component of this._elemsToComponents.values()) {
			if (node.contains(component.root)) {
				component.destroy();
				componentElementsToRemove.push(component.root);
			}
		}
		for (const element of componentElementsToRemove) {
			this._elemsToComponents.delete(element);
		}

		// Remove the node from its parent.
		if (node.parentNode !== null) {
			node.parentNode.removeChild(node);
		}
	}

	/** Clears all children of a node. */
	protected clearNode(node: Node): void {

		// Verify the node.
		if (!this.isNodeInThis(node)) {
			throw new Error('The node is not in this component.');
		}

		for (const child of [...node.childNodes]) {
			if (child instanceof Element && this._elemsToComponents.has(child)) {
				this.removeComponent(this._elemsToComponents.get(child)!);
			}
			else {
				this.removeNode(child);
			}
		}
	}

	/** Sets a new component of type *ComponentType* as a child of *parentNode* right before
	 * the child *beforeChild* using the *params*. */
	protected insertComponent<T extends Component>(ComponentType: new (params: Params) => T, parentNode: Node = this.root,
		before?: Node | Component, params: Omit<Partial<Params>, 'parent'> = {}): T {

		// Verify the parent and before params.
		if (!this.isNodeInThis(parentNode)) {
			throw new Error('The parent node is not in this component.');
		}
		if (before instanceof Component) {
			if (!this._elemsToComponents.has(before.root)) {
				throw new Error('The before component is not in this component.');
			}
			before = before.root;
		}
		else if (before && !this.isNodeInThis(before)) {
			throw new Error('The before node is not in this component.');
		}

		// Assign the default params.
		const paramsWithDefaults: Params = {
			attributes: new Map(),
			classes: [],
			eventHandlers: new Map(),
			innerHtml: '',
			innerHtmlContext: undefined,
			parent: this,
			...params
		};

		// Create the component.
		const newComponent = new ComponentType(paramsWithDefaults);

		// Insert the new component root in the right spot in this component.
		parentNode.insertBefore(newComponent.root, before ?? null);

		// Make sure any id attributes are made unique.
		this.makeIdsUnique(newComponent.root);

		// Add it to the list of components.
		this._elemsToComponents.set(newComponent.root, newComponent);

		// Return it.
		return newComponent;
	}

	/** Removes a component from this. */
	protected removeComponent(component: Component): void {

		// Verify the component.
		if (!this._elemsToComponents.has(component.root)) {
			throw new Error('The component is not in this component.');
		}

		// Delete the component from the list and destroy it.
		this._elemsToComponents.delete(component.root);
		component.destroy();

		// Remove the node from its parent.
		if (component.root.parentNode !== null) {
			component.root.parentNode.removeChild(component.root);
		}
	}

	/** Makes any id of the element unique within the base element, along with any connecting pairs. Checks children as well.
	 * Don't call this in the constructor, because the component hasn't yet been attached to the DOM. */
	protected makeIdsUnique(element: Element): void {

		// If it has an unprocessed id, make it unique.
		if (element.id !== '' && element.id.match(/-[0-9a-f]{16}$/u) === null) {

			// Get the base element for searching. It'll be an ancestor up to the closest '.uniqueIds' element, going back to the body.
			let baseElement = element;
			while (baseElement !== document.body && baseElement.parentElement !== null && !baseElement.classList.contains('uniqueIds')) {
				baseElement = baseElement.parentElement;
			}

			// Apply a unique suffix to the element's id.
			const id = element.id;
			const uniqueSuffix = `-${RandomString.generate(16)}`;
			element.id += uniqueSuffix;

			// Search for any of the listed attributes within the baseElement that have the id value,
			// and append the unique suffix to them.
			const attributes = ['for', 'aria-labelledby', 'aria-describedby'];
			for (const attribute of attributes) {
				const elems = baseElement.querySelectorAll(`[${attribute}="${id}"]`);
				for (const elem of elems) {
					elem.setAttribute(attribute, id + uniqueSuffix);
				}
			}
		}
		for (const child of element.childNodes) {
			if (child instanceof Element) {
				this.makeIdsUnique(child);
			}
		}
	}

	/** Returns true if the node is within this component, and not within any further child component. */
	private isNodeInThis(node: Node): boolean {
		while (true) {
			if (node === this.root) {
				return true;
			}
			if (node instanceof Element && node.classList.contains('Component')) {
				return false;
			}
			if (node.parentNode === null) {
				return false;
			}
			node = node.parentNode;
		}
	}

	/** Goes through all of the tags, and for any that match a component in the registry, sets it with
	 *  the matching component. Goes through all of the children also.
	 *  Returns the element that was created, if any. */
	private setComponentsAndEventHandlers(element: HTMLElement, context: Component | undefined): HTMLElement {
		const registryEntry = Component._registry.get(element.tagName.toLowerCase());
		// The element is a component ready to be instantiated. It can't be the root or a reserved tag.
		if (element !== this.root && element instanceof HTMLUnknownElement && registryEntry !== undefined) {

			// Create the params.
			const params = {
				parent: this,
				beforeNode: element,
				classes: [] as string[],
				attributes: new Map(),
				eventHandlers: new Map(),
				innerHtml: '',
				innerHtmlContext: context
			};

			// Add any classes that are from the element itself.
			for (const className of element.classList) {
				params.classes.push(className);
			}

			// Extract the event handlers from the attributes.
			if (context) {
				const eventHandlers = Component.extractEventHandlers(element, context);
				for (const entry of eventHandlers) {
					params.eventHandlers.set(entry[0], entry[1]);
				}
			}

			// Go through any remaining attributes and add them to the params.
			for (const attribute of element.attributes) {
				if (attribute.name.toLowerCase() === 'class') {
					continue;
				}
				params.attributes.set(attribute.name.toLowerCase(), attribute.value);
			}

			// Get the innerText and clear it.
			params.innerHtml = element.innerHTML;
			element.innerHTML = '';

			// Create the new component.
			let newComponent: Component;
			if (element !== this.root) {

				// Replace the element with the new component.
				newComponent = this.insertComponent(registryEntry.ComponentType, element.parentNode!, element, params);
				element.before(newComponent.root);
				element.remove();
			}
			// If element is the root, we don't need to insert it anywhere, just return it (it will be replaced in the constructor).
			else {

				// Create the component.
				newComponent = new registryEntry.ComponentType(params);

				// Add it to the list of components.
				this._elemsToComponents.set(newComponent.root, newComponent);
			}

			// Return the component's root.
			return newComponent.root;
		}
		else {

			// Extract the event handlers from the attributes and add the event listeners.
			if (context) {
				const eventHandlers = Component.extractEventHandlers(element, context);
				for (const entry of eventHandlers) {
					element.addEventListener(entry[0], entry[1]);
				}
			}

			// Go through the child elements.
			for (const child of element.children) {
				if (child instanceof HTMLElement) {
					this.setComponentsAndEventHandlers(child, context);
				}
			}

			// Return the existing element, since it didn't change.
			return element;
		}
	}

	/** Extracts the event handlers of the element as a mapping of strings to this-bound functions. */
	private static extractEventHandlers(element: Element, context: Component | undefined): Map<string, () => void> {
		const eventHandlers = new Map<string, () => void>();
		const attributesToRemove: string[] = [];
		// Get the attributes and event handlers.
		for (const attribute of element.attributes) {
			const attributeName = attribute.name.toLowerCase();
			if (attributeName.startsWith('on')) {
				const attributeParams: string[] = attribute.value.replace(/\\\|/gu, '&#124;').split('|').map((v) => v.replace(/&#124;/gu, '|'));
				const attributeValue = attributeParams.shift()!;
				if (!context) {
					throw new Error(`The value "${attributeValue}" of the event handler ${attributeName} of component element ${element.id} was not given a context.`);
				}
				// Don't do anything if the value is just empty.
				if (attributeValue === '') {
					continue;
				}
				if (attributeValue in context) {
					const value = (context as unknown as Record<string, unknown>)[attributeValue];
					if (value instanceof Function) {
						const functionCall = value.bind(context, ...attributeParams) as (this: Component, ...attributeParams: string[]) => void;
						eventHandlers.set(attributeName.substring(2), functionCall);
						attributesToRemove.push(attribute.name);
					}
					else {
						throw new Error(`In ${context.constructor.name}, the value "${attributeValue}" of component element ${element.tagName}${element.id !== '' ? `.${element.id}` : ''} is not a function of ${context.constructor.name}.`);
					}
				}
				else {
					throw new Error(`In ${context.constructor.name}, the value "${attributeValue}" of the event handler "${attributeName}" of component element ${element.tagName}${element.id !== '' ? `.${element.id}` : ''} is not in ${context.constructor.name}.`);
				}
			}
		}
		// Remove the attributes that were processed.
		for (const attributeName of attributesToRemove) {
			element.removeAttribute(attributeName);
		}
		// Return the processed event handlers.
		return eventHandlers;
	}

	/** Registers a component. */
	private static register(): void {

		// If it's already registered, do nothing.
		if (this._registry.has(this.name.toLowerCase())) {
			return;
		}

		// Make it globally accessible in the console via a UI object.
		const windowWithComponents = window as WindowWithComponents;
		if (!windowWithComponents.UI) {
			windowWithComponents.UI = {};
		}
		windowWithComponents.UI[this.name] = this;

		// Get the ancestors.
		const ancestors: (typeof Component)[] = [];
		let ancestor: typeof Component = this;
		while (ancestor !== Component) {
			ancestors.push(ancestor);
			// We know the ancestor of a subclass of component is also a component.
			ancestor = Object.getPrototypeOf(ancestor);
		}
		ancestors.push(Component);

		// Register the ancestors.
		for (let i = 1, l = ancestors.length; i < l; i++) {
			ancestors[i]!.register();
		}

		// Find the most recent ancestor (including this) that has html.
		let html;
		for (let i = 0, l = ancestors.length; i < l; i++) {
			ancestor = ancestors[i]!;
			if (html === undefined && Object.hasOwn(ancestor, 'html')) {
				html = ancestor.html.replace(/>[\t\n]+</gu, '><').trim();
			}
		}
		if (html === undefined) {
			throw new Error(`There is no html found for ${this.name} or its ancestors.`);
		}

		// Populate the ancestor registry entries.
		const ancestorEntries: RegistryEntry[] = [];
		for (let i = 1, l = ancestors.length; i < l; i++) {
			ancestorEntries.push(this._registry.get(ancestors[i]!.name.toLowerCase())!);
		}

		let css;
		if (Object.hasOwn(this, 'css')) {
			css = this.css.trim();
		}

		// Create the registry entry.
		const registryEntry: RegistryEntry = {
			ComponentType: this,
			html,
			css,
			ancestorEntries,
			styleElem: undefined,
			styleElemUseCount: 0
		};
		ancestorEntries.unshift(registryEntry);

		// Set the registry entry.
		this._registry.set(this.name.toLowerCase(), registryEntry);

		// Register any child component types.
		for (const componentType of this.childComponentTypes) {
			componentType.register();
		}
	}

	/** The mapping of elements to child components. */
	private _elemsToComponents = new Map<Element, Component>();

	/** The registry entry. */
	private _registryEntry: RegistryEntry;

	/** The child component types that need to be registered. */
	protected static childComponentTypes: (typeof Component)[] = [];

	/** The HTML that goes with the component. */
	protected static html = '';

	/** The CSS that goes with the component. */
	protected static css = /* css */`
		*, *::before, *::after {
			box-sizing: border-box;
		}

		.vertical-align {
			display: flex;
			justify-content: center;
			flex-direction: column;
		}

		.no-select {
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}
		`;

	/** The registered components, mapped from string to Component type. */
	private static readonly _registry = new Map<string, RegistryEntry>();
}

/** The params of an element that will become a component. */
export type Params = {

	/** The parent component. */
	parent: Component | undefined;

	/** The classes of the component, if it has any. */
	classes: string[];

	/** The attributes passed as if it were <Component attrib=''...>. All of the keys are lower case. */
	attributes: Map<string, string>;

	/** The event handlers passed as if it were <Component onevent=''...>. All of the keys are lower case, without the 'on' part. */
	eventHandlers: Map<string, (...args: unknown[]) => never>;

	/** The innerHtml of the node as if it were <Component>${innerText}</Component>. */
	innerHtml: string;

	/** The context to use for the innerHtml. */
	innerHtmlContext: Component | undefined;
};

/** The registry entry that describes a component type. */
type RegistryEntry = {

	/** The component type. */
	ComponentType: new (params: Params) => Component;

	/** The HTML for the ComponentType. */
	html: string;

	/** The CSS for the ComponentType. */
	css: string | undefined;

	/** The ancestor registry entries, including ComponentType at 0 and Component as the last. */
	ancestorEntries: RegistryEntry[];

	/** The style element. */
	styleElem: HTMLStyleElement | undefined;

	/** The number of components using this style. Includes ComponentType and all its descendants. */
	styleElemUseCount: number;
};
