/** You must include this in your own project directly, but you can copy it from here. */

/** An html import. */
declare module '*.html' {
	const content: string;
	export default content;
}

/** A css import. */
declare module '*.css' {
	const content: string;
	export default content;
}

/** An svg import. */
declare module '*.svg' {
	const content: string;
	export default content;
}
