import { JsonHelper, type JsonObject, type JsonType } from '@orourley/pine-lib';
import { WS } from './ws';

/** A message sending and receiving WebSocket interface. */
export class WSJson {

	/** Constructs the WSJson object. */
	constructor() {

		this.ws.onMessageReceived = async (reader, writer): Promise<void> => {

			// Decode it as utf-8.
			const dataAsString = new TextDecoder('utf-8').decode(reader.readRest());

			// Get the response.
			let data: JsonType;
			try {
				data = JSON.parse(dataAsString);
			}
			catch (error) {
				throw new Error(`Response must be valid JSON. ${error}`);
			}
			if (!JsonHelper.isObject(data)) {
				throw new Error('Response must be an object.');
			}

			// Get the module of the request data.
			const moduleName = data['module'];
			if (!JsonHelper.isString(moduleName)) {
				throw new Error('data.module must be a string.');
			}

			// Get the command of the request data.
			const command = data['command'];
			if (!JsonHelper.isString(command)) {
				throw new Error('data.command must be a string.');
			}

			// Get the params of the command, if any.
			const params = data['params'] ?? {};
			if (!JsonHelper.isObject(params)) {
				throw new Error('data.params must be an object or undefined.');
			}

			// Get the handler and call it.
			const handlers = this.handlers.get(moduleName);
			if (handlers === undefined) {
				throw new Error(`data.module "${moduleName}" is an invalid module.`);
			}

			// Process the command and send a response.
			for (const handler of handlers) {
				try {
					const response = await handler(command, params);
					if (writer !== undefined) {
						const responseAsString = JSON.stringify({
							success: true,
							response
						});
						const responseData = new TextEncoder().encode(responseAsString);
						writer.writeArrayBuffer(responseData.buffer);
					}
				}
				catch (error) {
					if (error instanceof Error) {
						if (writer !== undefined) {
							const responseAsString = JSON.stringify({
								success: false,
								error: error.message
							});
							const responseData = new TextEncoder().encode(responseAsString);
							writer.writeArrayBuffer(responseData.buffer);
						}
					}
					else {
						throw error;
					}
				}
			}
		};
	}

	/** Destroys the object. */
	destroy(): void {
		this.ws.destroy();
	}

	/** Connects to a web socket. */
	async connect(url: string): Promise<void> {
		return this.ws.connect(url);
	}

	/** Closes the web socket connection. */
	close(): void {
		this.ws.close();
	}

	/** Returns true if the web socket is connected. */
	isConnected(): boolean {
		return this.ws.isConnected();
	}

	/** Returns true if the websocket is connecting or connected. */
	isConnectingOrConnected(): boolean {
		return this.ws.isConnectingOrConnected();
	}

	/** Sends the data along the web socket. Returns a promise resolving with response data. */
	async send(module: string, command: string, params: JsonObject): Promise<JsonType | void | undefined> {
		// Send the request.
		const responseReader = await this.ws.sendRequest(new TextEncoder().encode(JSON.stringify({
			module,
			command,
			params
		})).buffer);
		// Return the response as JSON.
		const data = JSON.parse(new TextDecoder().decode(responseReader.readRest())) as JsonType;
		// // Uncomment this to debug server commands and responses.
		// console.log(`Sent: ${module} ${command}`, params);
		// console.log(`Received:`, data);
		if (!JsonHelper.isObject(data)) {
			throw new Error('Response data is not an object.');
		}
		if (!JsonHelper.isBoolean(data['success'])) {
			throw new Error('Reponse data.success is not a boolean.');
		}
		if (!data['success']) {
			if (!JsonHelper.isString(data['error'])) {
				throw new Error('Response (fail) data.error is not a string.');
			}
			throw new Error(data['error']);
		}
		return data['response'];
	}

	/** Register a handler for receiving unsolicited messages. */
	registerHandler(module: string, handler: (command: string, params: JsonObject) => Promise<JsonType | void>): void {
		if (this.handlers.has(module)) {
			this.handlers.get(module)!.push(handler);
		}
		else {
			this.handlers.set(module, [handler]);
		}
	}

	/** Unregister a handler for receiving unsolicited messages. */
	unregisterHandler(module: string, handler: (command: string, params: JsonObject) => Promise<JsonType | void>): void {
		if (this.handlers.has(module)) {
			const list = this.handlers.get(module)!;
			const index = list.findIndex((h) => h === handler);
			if (index !== -1) {
				list.splice(index);
			}
			if (list.length === 0) {
				this.handlers.delete(module);
			}
		}
	}

	/** Sets a callback that is called when the connection is closed. */
	setCloseCallback(closeCallback: () => void): void {
		this.ws.onConnectionClosed = closeCallback;
	}

	/** The WebSocket connection. */
	private ws = new WS();

	/** The handlers for received messages, separated by modules. */
	private handlers = new Map<string, ((command: string, params: JsonObject) => Promise<JsonType | void>)[]>();
}
