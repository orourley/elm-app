/** A router that supports query strings, pushing, and replacing using the history API. */
export class Router {

	/** Starts the router. Calls the initial query callbacks, using {} as the oldQuery. */
	async start(): Promise<void> {

		// Bind functions to this.
		this._processEvent = this._processEvent.bind(this);

		// Add an event listener so that it processes an event when the user uses the History API
		// or clicks a link that changes the hash.
		window.addEventListener('popstate', this._processEvent);
		window.addEventListener('hashchange', this._processEvent);

		// Get the current query.
		this._query = this._parseURL();

		// Call the callbacks once with the oldQuery set to an empty object.
		this._callCallbacks(undefined);
	}

	destroy(): void {
		// Clean up the event listeners.
		window.removeEventListener('popstate', this._processEvent);
		window.removeEventListener('hashchange', this._processEvent);
	}

	/** Adds the callback for when the URL query params have changed. */
	addCallback(callback: Router.Callback): void {
		this._callbacks.add(callback);
	}

	/** Removes the callback for when the URL query params have changed. */
	removeCallback(callback: Router.Callback): void {
		this._callbacks.delete(callback);
	}

	/** Gets a new query that combines with the existing query. */
	getRouterString(newQuery: Router.Query): string {
		return this._createQueryString({ ...this._query ?? {}, ...newQuery });
	}

	/** Pushes a query to the history and process it, calling the callback. \
	 *  All options default to false if omitted.
	 *  * If mergeOldQuery is true, the new query will be merged into the old query, overwriting any key-value pairs.
	 *  * If overwriteHistory is true, the browser history will not add a new entry, but will overwrite the previous entry.
	 *  * If noCallbacks is true, the callbacks will not be called. */
	setQuery(query: Router.Query, options?: { mergeOldQuery?: boolean; overwriteHistory?: boolean; noCallbacks?: boolean; }): void {

		// If there is no old query yet, that means the router hasn't started, so do nothing.
		if (!this._query) {
			return;
		}

		// Get the query and its corresponding string.
		const oldQuery = this._query;
		if (options?.mergeOldQuery === true) {
			this._query = { ...this._query, ...query };
		}
		else {
			this._query = { ...query };
		}
		const queryString = this._createQueryString(this._query);

		// Push or replace the query.
		const historyDoState = options?.overwriteHistory === true ? history.replaceState : history.pushState;
		historyDoState.call(history, undefined, '', queryString.length !== 0 ? `#${queryString}` : './');

		// Call the callbacks.
		if (options?.noCallbacks !== true) {
			this._callCallbacks(oldQuery);
		}
	}

	/** Called when a browser event triggers a URL change. */
	private _processEvent(): void {
		// Get the old and new query.
		const oldQuery = this._query;
		this._query = this._parseURL();

		// Check if they are identical.
		let queryChanged = false;
		if (oldQuery) {
			for (const key of Object.keys(oldQuery)) {
				if (oldQuery[key] !== this._query[key]) {
					queryChanged = true;
					break;
				}
			}
		}
		for (const key of Object.keys(this._query)) {
			if (oldQuery?.[key] !== this._query[key]) {
				queryChanged = true;
				break;
			}
		}

		// If different, call the callbacks.
		if (queryChanged) {
			this._callCallbacks(oldQuery);
		}
	}

	/** Parses the URL and returns a Query object. */
	private _parseURL(): Router.Query {
		const query: Router.Query = {};
		const hash = location.hash.substring(1);
		const entries = hash.split('&');
		if (entries.length > 1 || entries[0] !== '') {
			for (const entry of entries) {
				const [key, value] = entry.split('=', 2);
				if (key !== undefined && value !== undefined) {
					query[decodeURIComponent(key)] = decodeURIComponent(value);
				}
			}
		}
		return query;
	}

	/** Turns a query into a string suitable for a URL. */
	private _createQueryString(query: Router.Query): string {
		let queryString = '';
		for (const key in query) {
			if (!Object.hasOwn(query, key)) {
				continue;
			}
			const value = query[key];
			if (value === undefined) {
				continue;
			}
			if (queryString !== '') {
				queryString += '&';
			}
			queryString += `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
		}
		return queryString;
	}

	/** Calls all of the callbacks with the current query. */
	private _callCallbacks(oldQuery: Router.Query | undefined): void {

		// Copy the query in case it is modified.
		const query = { ...this._query };

		// Add the callbacks and the query to the list of callbacks.
		for (const callback of this._callbacks) {
			this._queuedCallbacks.push({ callback, query });
		}

		// If we're not currently calling callbacks, start that process.
		if (!this._callingCallbacks) {
			this._callingCallbacks = true;
			while (this._queuedCallbacks.length > 0) {
				const { callback, query: callbackQuery } = this._queuedCallbacks.shift()!;
				try {
					callback(oldQuery, callbackQuery);
				}
				catch (error: unknown) {
					if (error instanceof Error) {
						// eslint-disable-next-line no-console
						console.log('While parsing query', callbackQuery);
						// eslint-disable-next-line no-console
						console.log(`Error: ${error.message}`);
					}
					// eslint-disable-next-line no-console
					console.trace(error);
				}
			}
			this._callingCallbacks = false;
		}
	}

	/** The current query. */
	private _query: Router.Query | undefined;

	/** The callback to be called when the query changes. */
	private _callbacks = new Set<Router.Callback>();

	/** The queued callbacks. */
	private _queuedCallbacks: { callback: Router.Callback; query: Router.Query; }[] = [];

	/** Whether or not we're currently calling callbacks. */
	private _callingCallbacks: boolean = false;
}

export namespace Router {

	/** The query accepted by the router. */
	export type Query = Record<string, string | undefined>;

	/** The callback format when processing a query. */
	export type Callback = ((oldQuery: Query | undefined, newQuery: Query) => void);
}
