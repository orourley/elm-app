import { Reader, UniqueIds, Writer } from '@orourley/pine-lib';

/** A message sending and receiving WebSocket interface. */
export class WS {

	/** The callback called when a message is received. If it has a writer, it expects a response. */
	onMessageReceived: (reader: Reader, writer: Writer | undefined) => Promise<void> = async () => Promise.resolve();

	/** The callback called when there is a new connection. */
	onConnectionOpen: () => void = () => {};

	/** The callback called when the connection is closed. */
	onConnectionClosed: () => void = () => {};

	/** Connects to a web socket. */
	async connect(url: string): Promise<void> {

		// Setup the promise that resolves when the web socket is ready to be used.
		return new Promise((resolve: () => void, reject: (error: Error) => void) => {

			// Close any previous websocket.
			if (this.webSocket !== undefined) {
				this.webSocket.close();
			}

			// Reset the pings.
			this.pingsSent = 0;

			// Create the web socket.
			this.webSocket = new WebSocket(url);
			this.webSocket.binaryType = 'arraybuffer';

			// A flag that says we're still connecting.
			let connecting = true;

			// When the websocket connects...
			this.webSocket.onopen = (): void => {
				connecting = false;
				this.ping();
				resolve();
			};

			// When a message is received...
			this.webSocket.onmessage = async (message: MessageEvent): Promise<void> => {

				// Check if the websocket exists.
				if (this.webSocket === undefined) {
					return;
				}

				// Check for a pong.
				if (message.data === 'pong') {
					this.pingsSent -= 1;
					return;
				}

				try {

					// Make sure it's an ArrayBuffer.
					if (!(message.data instanceof ArrayBuffer)) {
						throw new Error('Error while receiving websocket message: the message data must be an ArrayBuffer.');
					}

					// Get the message data.
					const reader = new Reader(message.data);
					const id = reader.readInt32();

					// This is a message from the server, not expecting any response.
					if (id === 0) {

						// Call the callback.
						await this.onMessageReceived(reader, undefined);
					}

					// If this is a response from a request from the client.
					else if (id < 0) {

						// Get the function to be resolved using the response id.
						const promiseFunction = this.awaitingResponses.get(-id);

						// Remove the id from the actively sending message list and release the id.
						this.awaitingResponses.delete(-id);
						this.uniqueIds.release(-id - 1);

						// Call the promise resolve function.
						promiseFunction?.(reader);
					}

					// If this is a request from the server, expecting a response.
					else {

						// Start the response with a negative of the id, so that the receiver knows
						// it is the corresponding response.
						const writer = new Writer();
						writer.writeInt32(-id);

						// Process the message and send the response.
						await this.onMessageReceived(reader, writer);
						this.webSocket.send(writer.getData());
					}
				}
				catch (error) {
					throw new Error(`Error while receiving websocket message. Message: ${message.data} Error: ${error}`);
				}
			};

			// When the websocket disconnects...
			this.webSocket.onclose = (closeEvent: CloseEvent): void => {
				if (connecting) {
					connecting = false;
					reject(new Error(`${closeEvent.code} ${closeEvent.reason}.`));
				}
				else {
					this.onConnectionClosed();
				}
			};

			// Make sure to close the websocket before unloading the page.
			window.addEventListener('beforeunload', () => {
				if (this.webSocket !== undefined) {
					this.webSocket.close();
					this.webSocket = undefined;
				}
			});
		});
	}

	/** Destroys the object. */
	destroy(): void {
		this.close();
	}

	/** Closes the web socket connection. */
	close(): void {
		if (this.webSocket !== undefined) {
			this.webSocket.close();
			this.webSocket = undefined;
		}
	}

	/** Returns true if the web socket is connected. */
	isConnected(): boolean {
		return this.webSocket !== undefined && this.webSocket.readyState === WebSocket.OPEN;
	}

	/** Returns true if the websocket is connecting or connected. */
	isConnectingOrConnected(): boolean {
		return this.webSocket !== undefined;
	}

	/** Sends the data along the web socket. Does not wait for a response. */
	send(data: ArrayBuffer): void {
		if (this.webSocket === undefined || this.webSocket.readyState !== WebSocket.OPEN) {
			throw new Error('The web socket is not yet connected.');
		}
		const dataWriter = new Writer();
		dataWriter.writeInt32(0); // 0 means this is not a request/response.
		dataWriter.writeArrayBuffer(data);
		this.webSocket.send(dataWriter.getData());
	}

	/** Sends the data along the web socket. Returns a promise resolving with response data. */
	async sendRequest(data: ArrayBufferLike): Promise<Reader> {
		let requestId = 0;
		const reader = await new Promise<Reader>((resolve) => {
			if (this.webSocket === undefined || this.webSocket.readyState !== WebSocket.OPEN) {
				throw new Error('The web socket is not yet connected.');
			}
			requestId = 1 + this.uniqueIds.get();
			this.awaitingResponses.set(requestId, resolve);
			const dataWriter = new Writer();
			dataWriter.writeInt32(requestId);
			dataWriter.writeArrayBuffer(data);
			this.webSocket.send(dataWriter.getData());
		});
		return reader;
	}

	/** Send out a ping and wait for a pong as a keep-alive mechanism. */
	private ping(): void {

		// If there is no connect, don't ping.
		if (this.webSocket === undefined || this.webSocket.readyState !== WebSocket.OPEN) {
			return;
		}

		// If we've sent out enough pings and heard nothing back, the server connection is gone.
		if (this.pingsSent >= 2) {
			this.webSocket.close(1001, 'Too many pings to server without response pongs.');
			return;
		}

		// Send out another ping.
		this.webSocket.send('ping');
		this.pingsSent += 1;

		// Wait for some seconds before doing another ping.
		setTimeout(this.ping.bind(this), 20000);
	}

	/** The WebSocket connection. */
	private webSocket: WebSocket | undefined;

	/** A collection of unique ids for pairing requests and responses. */
	private uniqueIds: UniqueIds = new UniqueIds();

	/** The list of sent messages awaiting a response. */
	private awaitingResponses = new Map<number, (reader: Reader) => void>();

	/** Number of pings that have been sent without received pongs. */
	private pingsSent: number = 0;
}
