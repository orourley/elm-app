import { wait } from '@orourley/pine-lib';

interface ElementWithStyle extends Element {
	style: CSSStyleDeclaration;
}

/** A module for showing and hiding elements on a page. Since it uses dynamic styles, you must use the inline style "display: none", and not classes. */
export class ShowHide {

	/** Shows an element in an animated way over the duration. The element must be a block display style. */
	static async show(element: ElementWithStyle, durationInSeconds = 0.125): Promise<void> {
		if (getComputedStyle(element).display === 'none') {
			// Sets an attribute so anything else can tell its showing.
			element.setAttribute('showing', '1');
			// Set overflow to auto so that any child element margins don't leak out of the element.
			element.style.overflow = 'auto';
			// Get the height as a pixel value so that the transition works.
			const height = this._getHeight(element);
			// Blank everything out.
			element.style.opacity = '0';
			element.style.height = '0';
			element.style.marginTop = '0';
			element.style.marginBottom = '0';
			element.style.display = 'block';
			// Do a quick wait to ensure the above happens without a transition.
			await wait(0);
			// Start the transition to expand everything, while staying invisible.
			element.style.transition = `all ${durationInSeconds / 2}s`;
			element.style.height = `${height}px`;
			element.style.marginTop = '';
			element.style.marginBottom = '';
			await wait(durationInSeconds / 2);
			// Then become visible.
			element.style.opacity = '';
			await wait(durationInSeconds / 2);
			// At the end, clear the styles.
			element.style.height = '';
			element.style.transition = '';
			element.style.overflow = '';
			element.removeAttribute('showing');
			// One more quick wait to clear the transition effect.
			await wait(0);
		}
		return Promise.resolve();
	}

	/** Hides an element in an animated way over the duration. The element must be a block display style. */
	static async hide(element: ElementWithStyle, durationInSeconds = 0.125): Promise<void> {
		if (getComputedStyle(element).display !== 'none') {
			// Sets an attribute so anything else can tell its hiding.
			element.setAttribute('hiding', '1');
			// Set overflow to auto so that any child element margins don't leak out of the element.
			element.style.overflow = 'auto';
			// Set the height as a pixel value so that the transition works.
			const height = this._getHeight(element);
			element.style.height = `${height}px`;
			// Make it invisible.
			element.style.transition = `all ${durationInSeconds / 2}s`;
			element.style.opacity = '0';
			await wait(durationInSeconds / 2);
			// Shrink it down.
			element.style.height = '0';
			element.style.marginTop = '0';
			element.style.marginBottom = '0';
			await wait(durationInSeconds / 2);
			// At the end, hide the element and clear the styles.
			element.style.transition = '';
			element.style.display = 'none';
			element.style.opacity = '';
			element.style.height = '';
			element.style.marginTop = '';
			element.style.marginBottom = '';
			element.style.overflow = '';
			element.removeAttribute('hiding');
			// One more quick wait to clear the transition effect.
			await wait(0);
		}
		return Promise.resolve();
	}

	/** Toggles an element in an animated way over the duration. The element must be a block display style. */
	static async toggle(element: ElementWithStyle, durationInSeconds = 0.125): Promise<void> {
		if (this.isShown(element)) {
			return this.hide(element, durationInSeconds);
		}
		else {
			return this.show(element, durationInSeconds);
		}
	}

	/** Returns true if this is shown or showing. */
	static isShown(element: ElementWithStyle): boolean {
		return getComputedStyle(element).display !== 'none';
	}

	/** Returns true if this is hidden or hiding. */
	static isHidden(element: ElementWithStyle): boolean {
		return !this.isShown(element);
	}

	/** Fades the element in, only using opacity. */
	static async fadeIn(element: ElementWithStyle, durationInSeconds = 0.125): Promise<void> {
		element.style.transition = `opacity ${durationInSeconds}s`;
		element.style.opacity = '1';
		await wait(durationInSeconds);
		element.style.transition = '';
		element.style.opacity = '';
	}

	/** Fades the element out, only using opacity. */
	static async fadeOut(element: ElementWithStyle, durationInSeconds = 0.125): Promise<void> {
		element.style.transition = `opacity ${durationInSeconds}s`;
		element.style.opacity = '0';
		await wait(durationInSeconds);
		element.style.transition = '';
	}

	/** Gets the height of an element. */
	private static _getHeight(element: ElementWithStyle): number {
		const prevDisplay = element.style.display;
		element.style.display = 'block';
		const height = element.scrollHeight;// + style.marginTop + style.marginBottom;
		element.style.display = prevDisplay;
		return height;
	}
}
