import { wait } from '@orourley/pine-lib';

/** Class that supports opening and closing popups. */
export class PopupHelper {

	/** Opens a popup. Clicking anywhere else will close it. Returns promise that resolves when it is open. */
	static async open(elem: HTMLElement, elemsToIgnore: HTMLElement[] = [], closeCallback?: (elem: HTMLElement) => void): Promise<void> {
		if (!elem.classList.contains('on')) {

			// Add the 'on' class.
			elem.classList.add('on');

			// Wait a small amount until it is open.
			await wait(0.125);

			// Find the first scrollable ancestor.
			let scrollableNode: HTMLElement | null = elem.parentElement;
			while (scrollableNode !== null
				&& (['visible', 'hidden'].includes(window.getComputedStyle(scrollableNode).overflowY)
					|| scrollableNode.scrollHeight <= scrollableNode.clientHeight)) {
				scrollableNode = scrollableNode.parentElement;
			}

			// If one exists, scroll up or down to make the popup come into view.
			if (scrollableNode !== null) {
				if (elem.getBoundingClientRect().bottom > scrollableNode.getBoundingClientRect().bottom) {
					elem.scrollIntoView({
						behavior: 'smooth',
						block: 'end'
					});
				}
				if (elem.getBoundingClientRect().top < scrollableNode.getBoundingClientRect().top) {
					elem.scrollIntoView({
						behavior: 'smooth',
						block: 'start'
					});
				}
			}

			// Setup the info for the popup element.
			const popupInfo: PopupInfo = {
				elemsToIgnore: [elem, ...elemsToIgnore],
				closeCallback,
				windowEventListener: PopupHelper.onClose.bind(window, elem)
			};
			this._elemsToPopupInfos.set(elem, popupInfo);

			// Add the window event listener for closing the popup.
			window.addEventListener('mousedown', popupInfo.windowEventListener);
			window.addEventListener('touchstart', popupInfo.windowEventListener);
		}
	}

	/** Closes a popup. */
	static close(elem: HTMLElement): void {
		PopupHelper._close(elem);
	}

	/** Returns true if a popup is open. */
	static isOpen(elem: HTMLElement): boolean {
		return elem.classList.contains('on');
	}

	/** Called by window 'mousedown' or 'touchstart' event. */
	private static onClose(elem: HTMLElement, event: MouseEvent | TouchEvent): void {

		// Get the info for the popup element.
		const popupInfo = PopupHelper._elemsToPopupInfos.get(elem);
		if (popupInfo !== undefined) {

			let insideAtLeastOneElem = false;
			for (const elemToIgnore of popupInfo.elemsToIgnore) {
				// If the click is outside of the popup bounds, close the popup.
				const bounds = elemToIgnore.getBoundingClientRect();
				let x = 0;
				let y = 0;
				if (event instanceof MouseEvent) {
					x = event.x;
					y = event.y;
				}
				else if (event.touches.length > 0) {
					x = event.touches[0]!.clientX;
					y = event.touches[0]!.clientY;
				}
				if (bounds.left <= x && x <= bounds.right
					&& bounds.top <= y && y <= bounds.bottom) {
					insideAtLeastOneElem = true;
					break;
				}
			}

			if (!insideAtLeastOneElem) {
				PopupHelper._close(elem);
			}
		}
	}

	/** Closes the popup elem. */
	private static _close(elem: HTMLElement): void {

		// Get the info for the popup element.
		const popupInfo = PopupHelper._elemsToPopupInfos.get(elem);
		if (popupInfo !== undefined) {

			// Remove the popup elem's info.
			PopupHelper._elemsToPopupInfos.delete(elem);

			// Remove the 'on' class.
			elem.classList.remove('on');

			// Remove the window event listener for closing the popup.
			window.removeEventListener('mousedown', popupInfo.windowEventListener);
			window.removeEventListener('touchstart', popupInfo.windowEventListener);

			// Call the close callback.
			if (popupInfo.closeCallback !== undefined) {
				popupInfo.closeCallback(elem);
			}
		}
	}

	/** A mapping from popup elements to info about those elements. */
	private static _elemsToPopupInfos = new Map<HTMLElement, PopupInfo>();

	// /** The mapping of popup elems to user-specified close callbacks. */
	// private static _elemsToCloseCallbacks: Map<HTMLElement, (elem: HTMLElement) => void> = new Map();

	// /** The mapping of popup elems to callbacks so that the onClose knows which callback to remove from window. */
	// private static _elemsToEventListeners: Map<HTMLElement, (this: Window, ev: MouseEvent) => void> = new Map();
}

interface PopupInfo {

	/** The list of elements within which ignore clicks when deciding whether to close the popup. */
	elemsToIgnore: HTMLElement[];

	/** The user-specified close callback. */
	closeCallback: undefined | ((elem: HTMLElement) => void);

	/** Callback to be removed when the user clicks outside of the elem and ignore elems. */
	windowEventListener: (this: Window, ev: MouseEvent | TouchEvent) => void;
}
