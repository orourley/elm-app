/** A class that helps with throttling function calls. */
export class Throttle {

	/** Calls the callback, throttled.
	 *  @param callback - The callback function to call.
	 *  @param throttleTime - The minimum time to wait between subsequent callback calls. */
	call(callback: () => void, throttleTime: number = 250): void {

		// If we weren't planning on running, run it.
		if (!this._willRun) {

			// Get the time elapsed since the last run.
			const timeSinceLastRun = Date.now() - this._lastRunTime;

			// If we're over the throttle time, just run it normally.
			if (timeSinceLastRun >= throttleTime) {
				this._runCallback(callback);
			}

			// If we're under the throttle time, we need to wait until
			// the throttle time has elapsed before running it again.
			else {
				this._willRun = true;
				setTimeout(this._runCallback.bind(this, callback), throttleTime - timeSinceLastRun);
			}
		}
	}

	/** Runs the callback and clears the wait triggers. */
	private _runCallback(callback: () => void): void {
		this._willRun = false;
		this._lastRunTime = Date.now();
		callback();
	}

	/** A flag that indicates whether we're waiting to run. */
	private _willRun: boolean = false;

	/** The time of the last run. */
	private _lastRunTime: number = Number.NEGATIVE_INFINITY;
}
